Pod::Spec.new do |s|
  s.name         = "TFPhotoBrowser"
  s.version      = "1.0.0"
  s.summary      = "TF iOS照片浏览框架"
  s.homepage     = "https://gitee.com/Summer_c_xzx/TFPhotoBrowser.git"
  s.license      = "Copyright (C) 2018 TimeFace, Inc.  All rights reserved."
  s.author             = { "Summer" => "418463162@qq.com" }
  s.ios.deployment_target = "9.0"
  s.source       = { :git => "https://gitee.com/Summer_c_xzx/TFPhotoBrowser.git", :tag => s.version.to_s}
  s.source_files  = "TFPhotoBrowser/**/*.{h,m,c}"
  s.public_header_files = [
    'TFPhotoBrowser/Classes/**/*.h'
    ]
  s.resource = ['TFPhotoBrowser/Classes/Resource/TFPhotoResource.bundle']
  s.frameworks = 'UIKit', 'Foundation'
  s.weak_frameworks = 'Photos'
  s.requires_arc = true
  s.dependency 'SDWebImage'
end
