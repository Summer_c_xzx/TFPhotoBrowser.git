//
//  TFPhotoPickerCollectionViewSectionHeaderVIew.m
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/11/15.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#import "TFPhotoPickerCollectionViewSectionHeaderView.h"
#import "TFPhotoResourceHelper.h"
#import "UIColor+TFPhoto.h"

@interface TFPhotoPickerCollectionViewSectionHeaderView ()

/**
 主标题label
 */
@property (nonatomic, strong) UILabel *titleLabel;

/**
 副标题label
 */
@property (nonatomic, strong) UILabel *detailLabel;

/**
 模糊背景view
 */
@property (nonatomic, strong) UIVisualEffectView *backView;

/**
 全选按钮
 */
@property (nonatomic, strong) UIButton *selectAllButton;

@end

@implementation TFPhotoPickerCollectionViewSectionHeaderView

@synthesize model = _model;
@synthesize delegate = _delegate;

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.backView];
        [self addSubview:self.titleLabel];
        [self addSubview:self.detailLabel];
        [self addSubview:self.selectAllButton];
        
        //添加约束
        self.backView.translatesAutoresizingMaskIntoConstraints = NO;
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.detailLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.selectAllButton.translatesAutoresizingMaskIntoConstraints = NO;
        NSDictionary *viewDic = @{@"backView":self.backView,@"titleLabel":self.titleLabel,@"detailLabel":self.detailLabel,@"selectAllButton":self.selectAllButton};
        [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[backView]|" options:0 metrics:nil views:viewDic]];
        [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[backView]|" options:0 metrics:nil views:viewDic]];
        [NSLayoutConstraint constraintWithItem:self.titleLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0 constant:8.0].active = YES;
        [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5.0-[titleLabel]-0.0-[detailLabel]-5.0-|" options:NSLayoutFormatAlignAllLeft metrics:nil views:viewDic]];
        [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[detailLabel]-5.0-[selectAllButton]-8.0-|" options:0 metrics:nil views:viewDic]];
        [NSLayoutConstraint constraintWithItem:self.selectAllButton attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0].active = YES;
        
        [_selectAllButton setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        [_selectAllButton setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        
    }
    return self;
}

- (void)reloadHeaderView {
    //刷新数据
    if (self.model.locationTitle.length) {
        _titleLabel.text = self.model.locationTitle;
        _detailLabel.text = [NSString stringWithFormat:@"%@·%@我的娃决斗王景点藕带文件都我案件我都我阿吉豆我我骄傲id我骄傲id借我大奖哦id骄傲我觉得",self.model.dateString,self.model.locationDetail];
    }
    else {
        _titleLabel.text = self.model.dateString;
        _detailLabel.text = @"";
    }
    _selectAllButton.selected = (self.model.selectCount==self.model.items.count);
    
    if ([self.delegate respondsToSelector:@selector(photoPickerCollectionViewSectionHeaderViewDidReloadView:)]) {
        [self.delegate photoPickerCollectionViewSectionHeaderViewDidReloadView:self];
    }
}
- (void)selectAllButtonAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(photoPickerCollectionViewSectionHeaderView:didClickSelectAllButton:)]) {
        [self.delegate photoPickerCollectionViewSectionHeaderView:self didClickSelectAllButton:sender];
    }
}


#pragma mark - lazy load.

- (UIVisualEffectView *)backView {
    if (!_backView) {
        UIBlurEffect *effsect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
        _backView = [[UIVisualEffectView alloc] initWithEffect:effsect];
    }
    return _backView;
}

- (UIButton *)selectAllButton {
    if (!_selectAllButton) {
        _selectAllButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_selectAllButton setTitle:[TFPhotoResourceHelper localizedStringWithKey:@"全选"] forState:UIControlStateNormal];
        [_selectAllButton setTitleColor:[UIColor tfPhoto_hexColorWith:@"#069bf2"] forState:UIControlStateNormal];
        [_selectAllButton setTitle:[TFPhotoResourceHelper localizedStringWithKey:@"取消全选"] forState:UIControlStateSelected];
        [_selectAllButton setTitleColor:[UIColor tfPhoto_hexColorWith:@"#069bf2"] forState:UIControlStateSelected];
        _selectAllButton.titleLabel.font = [UIFont systemFontOfSize:16];
        CGFloat padding = 5.0;
        _selectAllButton.contentEdgeInsets = UIEdgeInsetsMake(padding, padding, padding, padding);
        [_selectAllButton addTarget:self action:@selector(selectAllButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _selectAllButton;
}

- (UILabel *)detailLabel {
    if (!_detailLabel) {
        _detailLabel = [[UILabel alloc] init];
        _detailLabel.font = [UIFont systemFontOfSize:12];
        _detailLabel.textColor = [UIColor blackColor];
    }
    return _detailLabel;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor blackColor];
        _titleLabel.font = [UIFont systemFontOfSize:14];
    }
    return _titleLabel;
}

@end
