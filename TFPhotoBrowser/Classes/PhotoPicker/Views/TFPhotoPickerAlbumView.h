//
//  TFPhotoPickerAlbumView.h
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/12/6.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TFPhotoPickerAlbumModel.h"

@class TFPhotoPickerAlbumView;
@class TFPhotoPickerItemModel;

/**
 图片选择相册弹框view代理
 */
@protocol TFPhotoPickerAlbumViewDelegate <NSObject>

@optional;

/**
 相册选择选择了某个相册列表

 @param photoPickerAlbumView 相册选择view
 @param indexPath 索引
 */
- (void)photoPickerAlbumView:(nonnull TFPhotoPickerAlbumView *)photoPickerAlbumView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath;

/**
 相册选择将要消失时调用

 @param photoPickerAlbumView 相册选择view
 */
- (void)photoPickerAlbumViewWillDissmiss:(nonnull TFPhotoPickerAlbumView *)photoPickerAlbumView;

@end

/**
 图片选择相册view
 */
@interface TFPhotoPickerAlbumView : UIView

/**
 显示方法

 @param momentAlbumModel 时刻相册数据
 @param selectedAlbumModel 选择相册数据
 @param mediaTypes 媒体类型
 @param delegate 代理
 */
+ (nonnull instancetype)showInView:(nonnull UIView *)view withMomentAlbumModel:(nonnull TFPhotoPickerAlbumModel *)momentAlbumModel
          selectedAlbumModel:(nonnull TFPhotoPickerAlbumModel *)selectedAlbumModel
                      mediaTypes:(nonnull NSArray *)mediaTypes
                        delegate:(nullable id<TFPhotoPickerAlbumViewDelegate>)delegate;

/**
 相册数据模型数组
 */
@property (nonatomic, strong, readonly, nonnull) NSArray<TFPhotoPickerAlbumModel *> *albumListArray;


/**
 分享类型的图片数据模型数组
 */
@property (nonatomic, strong, readonly, nonnull) NSMutableArray<TFPhotoPickerItemModel *> *shareTypeItemModelArray;

/**
 显示隐藏相册view

 @param hidden 隐藏
 */
- (void)showAlbumListTableViewWithHidden:(BOOL)hidden;

@end
