//
//  TFPhotoPickerVideoInfoView.m
//  Pods
//
//  Created by TFAppleWork-Summer on 2017/8/23.
//
//

#import "TFPhotoPickerVideoInfoView.h"
//工具
#import "TFPhotoResourceHelper.h"
#import "UIColor+TFPhoto.h"

@interface TFPhotoPickerVideoInfoView ()

@property (nonatomic, strong) UIImageView *logoImageView;

@property (nonatomic, strong) UILabel *lengthLabel;

@end

@implementation TFPhotoPickerVideoInfoView

- (void)setLengthInfo:(NSString *)lengthInfo {
    _lengthInfo = lengthInfo;
    _lengthLabel.text = lengthInfo;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.logoImageView];
        [self addSubview:self.lengthLabel];
        
        _logoImageView.translatesAutoresizingMaskIntoConstraints = NO;
        _lengthLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        NSDictionary *viewsDic = @{@"logoImageView":_logoImageView,@"lengthLabel":_lengthLabel};
        [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8.0-[logoImageView]-6.0-[lengthLabel]" options:NSLayoutFormatAlignAllCenterY metrics:nil views:viewsDic]];
        [NSLayoutConstraint constraintWithItem:_logoImageView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0].active = YES;
        
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if (CGRectGetHeight(self.frame)) {
        self.backgroundColor = [UIColor tfPhoto_gradientColorFromColor:[UIColor clearColor] toColor:[UIColor blackColor] withHeight:CGRectGetHeight(self.frame)];
    }
}

#pragma mark - lazy load.

- (UILabel *)lengthLabel {
    if (!_lengthLabel) {
        _lengthLabel = [[UILabel alloc] init];
        _lengthLabel.textColor = [UIColor whiteColor];
        _lengthLabel.font = [UIFont systemFontOfSize:12];
    }
    return _lengthLabel;
}

- (UIImageView *)logoImageView {
    if (!_logoImageView) {
        _logoImageView = [[UIImageView alloc] initWithImage:[TFPhotoResourceHelper imageNamed:@"photo_video"]];
        _logoImageView.contentMode = UIViewContentModeScaleAspectFill;
        _logoImageView.clipsToBounds = YES;
    }
    return _logoImageView;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
