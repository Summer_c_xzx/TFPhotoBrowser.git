//
//  TFPhotoPickerPublicProtocol.h
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/11/21.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#ifndef TFPhotoPickerPublicProtocol_h
#define TFPhotoPickerPublicProtocol_h

@class TFPhotoPickerMomentModel;
@protocol TFPhotoPickerCollectionViewSectionHeaderViewDelegate;

/**
 图片选择section header需要服从的协议
 */
@protocol TFPhotoPickerCollectionViewSectionHeaderViewPublic <NSObject>

@required;

/**
 对应数据模型
 */
@property (nonatomic, strong, nonnull) TFPhotoPickerMomentModel *model;

/**
 代理,传递事件
 */
@property (nonatomic, weak, nullable) id<TFPhotoPickerCollectionViewSectionHeaderViewDelegate>delegate;

/**
 刷新HeaderViewUI
 */
- (void)reloadHeaderView;

@end


/**
 图片选择 section header 的代理
 */
@protocol TFPhotoPickerCollectionViewSectionHeaderViewDelegate <NSObject>

@required;

/**
 点击选择全部按钮调用
 
 @param headerView section头部view
 @param sender 选择全部按钮
 */
- (void)photoPickerCollectionViewSectionHeaderView:(nonnull __kindof UICollectionReusableView<TFPhotoPickerCollectionViewSectionHeaderViewPublic> *)headerView didClickSelectAllButton:(nonnull UIButton *)sender;

@optional;

/**
 headerView根据数据模型刷新UI时调用
 
 @param headerView 头部view
 */
- (void)photoPickerCollectionViewSectionHeaderViewDidReloadView:(nonnull __kindof UICollectionReusableView<TFPhotoPickerCollectionViewSectionHeaderViewPublic> *)headerView;

@end

@class TFPhotoPickerItemModel;
@protocol TFPhotoPickerItemCollectionViewCellDelegate;

/**
 图片选择item需要服从的协议
 */
@protocol TFPhotoPickerItemCollectionViewCellPublic <NSObject>

/**
 图片view
 */
@property (nonatomic, strong, nonnull) UIImageView *photoImageView;

/**
 数据模型
 */
@property (nonatomic, strong, nonnull) TFPhotoPickerItemModel *model;

/**
 代理
 */
@property (nonatomic, weak, nullable) id<TFPhotoPickerItemCollectionViewCellDelegate> delegate;

/**
 重载cell，重载前数据模型需要改变
 */
- (void)reloadCell;

/**
 选择按钮点击时的动画实现

 @param animated animated
 */
- (void)doSelectWithAnimated:(BOOL)animated;

/**
 开始下载iCloud图片
 */
- (void)startDownloadICloudImage;

@end

/**
 图片选择item的代理
 */
@protocol TFPhotoPickerItemCollectionViewCellDelegate <NSObject>

@required;

/**
 cell上选择按钮的事件传递
 
 @param cell cell
 @param sender 选择按钮
 */
- (void)photoPickerItemCollectionViewCell:(nonnull __kindof UICollectionViewCell<TFPhotoPickerItemCollectionViewCellPublic> *)cell didClickSelectButton:(nonnull UIButton *)sender;

@optional;

/**
 cell刷新UI时调用
 
 @param cell UICollectionViewCell<TFPhotoPickerItemCollectionViewCellPublic>
 */
- (void)photoPickerItemCollectionViewCellDidReloadCell:(nonnull __kindof UICollectionViewCell<TFPhotoPickerItemCollectionViewCellPublic> *)cell;

/**
 cell成功从iCloud上下载完图片

 @param cell UICollectionViewCell<TFPhotoPickerItemCollectionViewCellPublic>
 */
- (void)photoPickerItemCollectionViewCellDidFinishDownloadImageFromICloud:(nonnull __kindof UICollectionViewCell<TFPhotoPickerItemCollectionViewCellPublic> *)cell;

@end

#endif /* TFPhotoPickerPublicProtocol_h */
