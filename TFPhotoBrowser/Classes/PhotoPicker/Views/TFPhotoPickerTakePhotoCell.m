
//
//  TFPhotoPickerTakePhotoCell.m
//  TimeFaceDemoProject
//
//  Created by TFAppleWork-Summer on 2017/5/16.
//  Copyright © 2017年 Summer. All rights reserved.
//

#import "TFPhotoPickerTakePhotoCell.h"
#import "TFPhotoResourceHelper.h"


@interface TFPhotoPickerTakePhotoCell ()

@property (nonatomic, strong) UIImageView *takePhotoImageView;

@end

@implementation TFPhotoPickerTakePhotoCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self.contentView addSubview:self.takePhotoImageView];
        self.contentView.backgroundColor = [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1.0];
        [NSLayoutConstraint constraintWithItem:self.takePhotoImageView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0].active = YES;
        [NSLayoutConstraint constraintWithItem:self.takePhotoImageView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0].active = YES;
    }
    return self;
}

- (UIImageView *)takePhotoImageView {
    if (!_takePhotoImageView) {
        _takePhotoImageView = [[UIImageView alloc] initWithImage:[TFPhotoResourceHelper imageNamed:@"photo_camera_take"]];
        _takePhotoImageView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _takePhotoImageView;
}


@end
