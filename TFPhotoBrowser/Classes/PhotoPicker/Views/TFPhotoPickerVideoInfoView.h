//
//  TFPhotoPickerVideoInfoView.h
//  Pods
//
//  Created by TFAppleWork-Summer on 2017/8/23.
//
//

#import <UIKit/UIKit.h>

/**
 图片选择Video的信息view
 */
@interface TFPhotoPickerVideoInfoView : UIView

/**
 时长信息
 */
@property (nonatomic, strong) NSString *lengthInfo;

@end
