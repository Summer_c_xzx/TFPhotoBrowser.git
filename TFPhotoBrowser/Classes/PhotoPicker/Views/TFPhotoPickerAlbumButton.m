//
//  TFPhotoPickerAlbumButton.m
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/11/17.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#import "TFPhotoPickerAlbumButton.h"

@implementation TFPhotoPickerAlbumButton

- (CGRect)contentRectForBounds:(CGRect)bounds {
    CGRect rect = [super contentRectForBounds:bounds];
    CGPoint origin = rect.origin;
    rect = UIEdgeInsetsInsetRect(rect, UIEdgeInsetsMake(-self.titleEdgeInsets.top, -self.titleEdgeInsets.left, -self.titleEdgeInsets.bottom, -self.titleEdgeInsets.right));
    rect = UIEdgeInsetsInsetRect(rect, UIEdgeInsetsMake(-self.imageEdgeInsets.top, -self.imageEdgeInsets.left, -self.imageEdgeInsets.bottom, -self.imageEdgeInsets.right));
    rect.origin = origin;
    return rect;
}

- (CGRect)imageRectForContentRect:(CGRect)contentRect {
    CGRect rect = [super imageRectForContentRect:contentRect];
    rect.origin.x = CGRectGetMaxX(contentRect) - CGRectGetWidth(rect);
    return rect;
}

- (CGRect)titleRectForContentRect:(CGRect)contentRect {
    CGRect rect = [super titleRectForContentRect:contentRect];
    rect.origin.x = 0;
    return rect;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
