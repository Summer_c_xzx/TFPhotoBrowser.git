//
//  TFPhotoPickerCollectionViewSectionHeaderVIew.h
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/11/15.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TFPhotoPickerPublicProtocol.h"
#import "TFPhotoPickerMomentModel.h"

/**
 图片选择section HeaderView
 */
@interface TFPhotoPickerCollectionViewSectionHeaderView : UICollectionReusableView<TFPhotoPickerCollectionViewSectionHeaderViewPublic>


@end
