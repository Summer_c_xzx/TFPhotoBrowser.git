//
//  TFPhotoAuthDeniedView.h
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/12/3.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 图片授权失败view
 */
@interface TFPhotoAuthDeniedView : UIView


@end
