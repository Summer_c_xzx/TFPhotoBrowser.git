//
//  TFPhotoPickerAlbumTableViewCell.m
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/11/16.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#import "TFPhotoPickerAlbumTableViewCell.h"
#import "TFPhotoHandler.h"
#import "TFPhotoPickerHelper.h"

const CGFloat kTFPhotoPickerAlbumThumnailImageSize = 40.0;

NSString * const kTFPhotoPickerAlbumTableViewCellIdentifier = @"TFPhotoPickerAlbumTableViewCell";

@interface TFPhotoPickerAlbumTableViewCell ()

@property (nonatomic, strong) UIImageView *albumImageView;
@property (nonatomic, strong) UILabel *albumNameLabel;
@property (nonatomic, strong) UILabel *albumAssetsCountLabel;

@end

@implementation TFPhotoPickerAlbumTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.albumImageView];
        [self.contentView addSubview:self.albumNameLabel];
        [self.contentView addSubview:self.albumAssetsCountLabel];
        
        //添加约束
        _albumImageView.translatesAutoresizingMaskIntoConstraints = NO;
        _albumNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _albumAssetsCountLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        [NSLayoutConstraint constraintWithItem:self.albumImageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:1.0 constant:kTFPhotoPickerAlbumThumnailImageSize].active = YES;
        [NSLayoutConstraint constraintWithItem:self.albumImageView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1.0 constant:kTFPhotoPickerAlbumThumnailImageSize].active = YES;
        [NSLayoutConstraint constraintWithItem:self.albumImageView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0].active = YES;
        [NSLayoutConstraint constraintWithItem:self.albumImageView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:15.0].active = YES;
        
        [NSLayoutConstraint constraintWithItem:self.albumNameLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.albumImageView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:-4.0].active = YES;
        [NSLayoutConstraint constraintWithItem:self.albumNameLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.albumImageView attribute:NSLayoutAttributeRight multiplier:1.0 constant:8.0].active = YES;
        [NSLayoutConstraint constraintWithItem:self.albumAssetsCountLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.albumImageView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:4.0].active = YES;
        [NSLayoutConstraint constraintWithItem:self.albumAssetsCountLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.albumNameLabel attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.0].active = YES;
        
    }
    return self;
}

- (void)setModel:(TFPhotoPickerAlbumModel *)model {
    _model = model;
    //获取缩略图
    if (!model.thumbnailImage) {
        [[TFPhotoHandler sharedHandler] getThumbnailImageWithAsset:[model.assets firstObject] size:[TFPhotoPickerHelper imageSize] resultHandler:^(UIImage *image, NSDictionary *info) {
            self.model.thumbnailImage = image;
            self.albumImageView.image = image;
        }];
    }
    else {
        self.albumImageView.image = model.thumbnailImage;
    }
    self.albumNameLabel.text = model.albumName;
    self.albumAssetsCountLabel.text = [NSString stringWithFormat:@"%zd",model.assetsCount];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - lazy load.

- (UILabel *)albumAssetsCountLabel {
    if (!_albumAssetsCountLabel) {
        _albumAssetsCountLabel = [[UILabel alloc] init];
        _albumAssetsCountLabel.textColor = [UIColor blackColor];
        _albumAssetsCountLabel.font = [UIFont systemFontOfSize:16];
    }
    return _albumAssetsCountLabel;
}
- (UILabel *)albumNameLabel {
    if (!_albumNameLabel) {
        _albumNameLabel = [[UILabel alloc] init];
        _albumNameLabel.textColor = [UIColor blackColor];
        _albumNameLabel.font = [UIFont systemFontOfSize:14];
    }
    return _albumNameLabel;
}

- (UIImageView *)albumImageView {
    if (!_albumImageView) {
        _albumImageView = [[UIImageView alloc] init];
        _albumImageView.contentMode = UIViewContentModeScaleAspectFill;
        _albumImageView.clipsToBounds = YES;
        _albumImageView.layer.cornerRadius = 5.0;
        _albumImageView.layer.masksToBounds = YES;
    }
    return _albumImageView;
}

@end
