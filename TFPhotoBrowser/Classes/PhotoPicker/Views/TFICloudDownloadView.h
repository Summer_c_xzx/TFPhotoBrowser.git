//
//  TFCoolDownloadView.h
//  MyCoolAnimationCollectionProject
//
//  Created by TFAppleWork-Summer on 2017/1/19.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TFICloudDownloadView;

/**
 ICloud图片加载完成block

 @param downloadView 下载view
 */
typedef void(^TFICloudDownloadAnimationCompletionBlock)(TFICloudDownloadView * __nonnull downloadView);

/**
 ICloud图片下载view
 */
@interface TFICloudDownloadView : UIView

/**
 开始动画
 */
- (void)startAnimationWithCompletion:(nullable TFICloudDownloadAnimationCompletionBlock)completion;

/**
 成功动画
 */
- (void)successAnimationWithCompletion:(nullable TFICloudDownloadAnimationCompletionBlock)completion;

/**
 失败动画
 */
- (void)failAnimation;

@property (nonatomic, assign) CGSize cloudSize;

/**
 进度
 */
@property (nonatomic, assign) float progress;

/**
 恢复到最开始的状态
 */
- (void)recoverOriginalState;

@end
