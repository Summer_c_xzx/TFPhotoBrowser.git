//
//  TFPhotoPickerAlbumTableViewCell.h
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/11/16.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TFPhotoPickerAlbumModel.h"

extern const CGFloat kTFPhotoPickerAlbumThumnailImageSize;

extern NSString * const kTFPhotoPickerAlbumTableViewCellIdentifier;

@interface TFPhotoPickerAlbumTableViewCell : UITableViewCell

@property (nonatomic, strong) TFPhotoPickerAlbumModel *model;

@end
