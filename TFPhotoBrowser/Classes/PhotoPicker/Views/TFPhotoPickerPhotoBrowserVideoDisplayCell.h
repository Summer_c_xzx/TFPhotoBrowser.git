//
//  TFPhotoPickerPhotoBrowserVideoDisplayCell.h
//  Pods
//
//  Created by TFAppleWork-Summer on 2017/8/24.
//
//

#import "TFPhotoBrowserVideoDisplayCell.h"
/**
 图片选择器中大图预览的视频cell
 */
@interface TFPhotoPickerPhotoBrowserVideoDisplayCell : TFPhotoBrowserVideoDisplayCell

/**
 选择按钮
 */
@property (nonatomic, strong, readonly, nonnull) UIButton *selectButton;

/**
 选择按钮方法调用
 */
@property (nonatomic, copy, nullable) void(^selectButtonActionBlock)(UIButton * __nonnull sender);

/**
 设置选择按钮的隐藏
 
 @param hidden BOOL
 */
- (void)setSelectButtonHidden:(BOOL)hidden;

/**
 设置选择按钮的选择
 
 @param select BOOL
 */
- (void)setSelectButtonSelect:(BOOL)select;

/**
 设置选择按钮的transform
 
 @param hidden 是否隐藏
 @param animated 是否使用动画
 */
- (void)setSelectButtonTransformWithNavigationBarHidden:(BOOL)hidden animated:(BOOL)animated;

@end
