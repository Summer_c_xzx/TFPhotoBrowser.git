//
//  TFICloudDownloadViewAnimationInfoObject.h
//  MyCoolAnimationCollectionProject
//
//  Created by TFAppleWork-Summer on 2017/2/6.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 ICloud图片加载view动画信息对象
 */
@interface TFICloudDownloadViewAnimationInfoObject : NSObject

/**
 frame
 */
@property (nonatomic, assign) CGRect frame;

/**
 云的路径
 */
@property (nonatomic, strong, nonnull) UIBezierPath *cloudPath;

/**
 竖线开始的路径
 */
@property (nonatomic, strong, nonnull) UIBezierPath *verticalLineStartPath;

/**
 竖线结束的路径
 */
@property (nonatomic, strong, nonnull) UIBezierPath *verticalLineEndPath;

/**
 箭头开始的路径
 */
@property (nonatomic, strong, nonnull) UIBezierPath *arrowStartPath;

/**
 箭头弹簧最下的路径
 */
@property (nonatomic, strong, nonnull) UIBezierPath *arrowBounceDownPath;

/**
 箭头弹簧嘴上的路径
 */
@property (nonatomic, strong, nonnull) UIBezierPath *arrowBounceUpPath;

/**
 箭头结束的路径
 */
@property (nonatomic, strong, nonnull) UIBezierPath *arrowEndPath;

/**
 箭头回到云的起点的路径
 */
@property (nonatomic, strong, nonnull) UIBezierPath *arrowCloudStartPath;

/**
 重新加载箭头路径
 */
@property (nonatomic, strong, nonnull) UIBezierPath *reloadArrowPath;

/**
 重新加载圆圈路径
 */
@property (nonatomic, strong, nonnull) UIBezierPath *reloadCirclePath;

/**
 成功路径
 */
@property (nonatomic, strong, nonnull) UIBezierPath *successPath;

/**
 初始化方法
 
 @param frame frame
 @return TFICloudDownloadView
 */
- (nonnull instancetype)initWithFrame:(CGRect)frame cloudSize:(CGSize)cloudSize;

/**
 圆圈加载中的动画
 
 @return CAKeyframeAnimation
 */
- (nonnull CAKeyframeAnimation *)circleLoadingAnimation;

/**
 线到点的动画
 
 @return CASpringAnimation
 */
- (nonnull CABasicAnimation *)lineToPointAnimation;

/**
 箭头到线的动画
 
 @return CAKeyframeAnimation
 */
- (nonnull CAKeyframeAnimation *)arrowToPointAnimation;

/**
 云到成功动画
 
 @return CABasicAnimation
 */
- (nonnull CABasicAnimation *)cloudToSuccessAnimation;

/**
 成功时伴随的圆圈动画
 
 @return CABasicAnimation
 */
- (nonnull CABasicAnimation *)successByCircleAnimation;

@end
