//
//  TFPhotoPickerCollectionViewCell.h
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/11/15.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TFPhotoPickerPublicProtocol.h"
#import "TFPhotoPickerItemModel.h"

/**
 图片选择显示item的cell
 */
@interface TFPhotoPickerItemCollectionViewCell : UICollectionViewCell<TFPhotoPickerItemCollectionViewCellPublic>

/**
 选择按钮
 */
@property (nonatomic, strong, readonly, nonnull) UIButton *selectButton;

/**
 添加子view的约束
 */
- (void)addSubViewContraints;

@end
