//
//  TFPhotoPickerLoadingView.m
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2017/1/18.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import "TFPhotoPickerLoadingView.h"
#import "TFPhotoResourceHelper.h"

@interface TFPhotoPickerLoadingView ()

@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;

@property (nonatomic, strong) UILabel *loadingLabel;

@end

@implementation TFPhotoPickerLoadingView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.indicatorView];
        [self addSubview:self.loadingLabel];
        
        _indicatorView.translatesAutoresizingMaskIntoConstraints = NO;
        _loadingLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        NSDictionary *viewsDic = @{@"indicatorView":self.indicatorView,@"loadingLabel":self.loadingLabel};
        
        [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[indicatorView]-0-[loadingLabel]-0-|" options:0 metrics:nil views:viewsDic]];
        [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[indicatorView]-0-|" options:0 metrics:nil views:viewsDic]];
        [NSLayoutConstraint constraintWithItem:self.loadingLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0].active = YES;
        
    }
    return self;
}

+ (instancetype)showInView:(UIView *)view {
    TFPhotoPickerLoadingView *loadingView = [[TFPhotoPickerLoadingView alloc] init];
    [view addSubview:loadingView];
    loadingView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [NSLayoutConstraint constraintWithItem:loadingView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0].active = YES;
    [NSLayoutConstraint constraintWithItem:loadingView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0].active = YES;
    [loadingView.indicatorView startAnimating];
    return loadingView;
}

- (void)dismiss {
    [self.indicatorView stopAnimating];
    [self removeFromSuperview];
}


#pragma mark - lazy load.

- (UIActivityIndicatorView *)indicatorView {
    if (!_indicatorView) {
        _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _indicatorView.hidesWhenStopped = YES;
    }
    return _indicatorView;
}

- (UILabel *)loadingLabel {
    if (!_loadingLabel) {
        _loadingLabel = [[UILabel alloc] init];
        _loadingLabel.textColor = [UIColor lightGrayColor];
        _loadingLabel.font = [UIFont systemFontOfSize:14];
        _loadingLabel.text = [TFPhotoResourceHelper localizedStringWithKey:@"拼命加载中..."];
    }
    return _loadingLabel;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
