//
//  TFPhotoAuthDeniedView.m
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/12/3.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#import "TFPhotoAuthDeniedView.h"
#import "TFPhotoResourceHelper.h"
#import "UIColor+TFPhoto.h"

@implementation TFPhotoAuthDeniedView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        //获取子view
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[TFPhotoResourceHelper imageNamed:@"photo_auth_denied"]];
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.text = [TFPhotoResourceHelper localizedStringWithKey:@"此应用没有权限访问您的照片或者视频"];
        titleLabel.font = [UIFont boldSystemFontOfSize:14];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.numberOfLines = 0;
        titleLabel.textColor = [UIColor tfPhoto_hexColorWith:@"#666666"];
        
        UILabel *detailLabel = [[UILabel alloc] init];
        detailLabel.text = [TFPhotoResourceHelper localizedStringWithKey:@"您可以在“隐私设置”中启用访问"];
        detailLabel.textColor = [UIColor tfPhoto_hexColorWith:@"#666666"];;
        detailLabel.font = [UIFont systemFontOfSize:12];

        UIButton *fastSettingButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [fastSettingButton setTitle:[TFPhotoResourceHelper localizedStringWithKey:@"快速设置权限"] forState:UIControlStateNormal];
        [fastSettingButton setTitleColor:[UIColor tfPhoto_hexColorWith:@"#999999"] forState:UIControlStateNormal];
        fastSettingButton.titleLabel.font = [UIFont systemFontOfSize:12];
        fastSettingButton.layer.masksToBounds = YES;
        fastSettingButton.layer.cornerRadius = 17.0;
        fastSettingButton.layer.borderColor = [UIColor tfPhoto_hexColorWith:@"#999999"].CGColor;
        fastSettingButton.layer.borderWidth = 1.0;
        [fastSettingButton addTarget:self action:@selector(p_fastSettingButtonAction) forControlEvents:UIControlEventTouchUpInside];
        
        //添加子view
        [self addSubview:imageView];
        [self addSubview:titleLabel];
        [self addSubview:detailLabel];
        [self addSubview:fastSettingButton];
        
        //添加约束
        imageView.translatesAutoresizingMaskIntoConstraints = NO;
        titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        detailLabel.translatesAutoresizingMaskIntoConstraints = NO;
        fastSettingButton.translatesAutoresizingMaskIntoConstraints = NO;
        NSDictionary *viewDic = @{@"titleLabel":titleLabel};
        
        
        [NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0].active = YES;
        [NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0].active = YES;
        
        [NSLayoutConstraint constraintWithItem:titleLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:imageView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:16.0].active = YES;
        
        [NSLayoutConstraint constraintWithItem:titleLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0].active = YES;
        [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10.0-[titleLabel]-10.0-|" options:0 metrics:nil views:viewDic]];
        
        [NSLayoutConstraint constraintWithItem:detailLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:titleLabel attribute:NSLayoutAttributeBottom multiplier:1.0 constant:12.0].active = YES;
        [NSLayoutConstraint constraintWithItem:detailLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0].active = YES;
        [NSLayoutConstraint constraintWithItem:fastSettingButton attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:detailLabel attribute:NSLayoutAttributeBottom multiplier:1.0 constant:16.0].active = YES;
        [NSLayoutConstraint constraintWithItem:fastSettingButton attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0].active = YES;
        [NSLayoutConstraint constraintWithItem:fastSettingButton attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:1.0 constant:210.0].active = YES;
        [NSLayoutConstraint constraintWithItem:fastSettingButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1.0 constant:34.0].active = YES;
        [NSLayoutConstraint constraintWithItem:fastSettingButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0].active = YES;
        
    }
    return self;
}

- (void)p_fastSettingButtonAction {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
