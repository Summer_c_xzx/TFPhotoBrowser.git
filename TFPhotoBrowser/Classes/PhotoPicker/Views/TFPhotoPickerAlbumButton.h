//
//  TFPhotoPickerAlbumButton.h
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/11/17.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 图片选择相册按钮
 */
@interface TFPhotoPickerAlbumButton : UIButton

@property (nonatomic, assign) BOOL changePostion;///<是否交换图片和标题的位置，默认YES

@end
