//
//  TFPhotoPickerPhotoBrowserDisplayCell.m
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2017/2/9.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import "TFPhotoPickerPhotoBrowserDisplayCell.h"
#import "TFPhotoResourceHelper.h"

@interface TFPhotoPickerPhotoBrowserDisplayCell ()

@property (nonatomic, strong, readwrite) UIButton *selectButton;

@end


@implementation TFPhotoPickerPhotoBrowserDisplayCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self.contentView addSubview:self.selectButton];
        _selectButton.hidden = YES;
        _selectButton.translatesAutoresizingMaskIntoConstraints = NO;
        [NSLayoutConstraint constraintWithItem:_selectButton attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTop multiplier:1.0 constant:64.0+10.0].active = YES;
        [NSLayoutConstraint constraintWithItem:_selectButton attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeRight multiplier:1.0 constant:-12.0].active = YES;
    }
    return self;
}

- (void)setSelectButtonHidden:(BOOL)hidden {
    self.selectButton.hidden = hidden;
}

- (void)setSelectButtonSelect:(BOOL)select {
    self.selectButton.selected = select;
}

- (void)p_selectButtonAction:(UIButton *)sender {
    if (_selectButtonActionBlock) {
        _selectButtonActionBlock(sender);
    }
}

- (void)setSelectButtonTransformWithNavigationBarHidden:(BOOL)hidden animated:(BOOL)animated{
    if (animated) {
        [UIView animateWithDuration:0.3 animations:^{
            [self p_setSelectButtonTransformWithNavigationBarHidden:hidden];
        }];
    }
    else {
        [self p_setSelectButtonTransformWithNavigationBarHidden:hidden];
    }
}

- (void)p_setSelectButtonTransformWithNavigationBarHidden:(BOOL)hidden {
    if (hidden) {
        self.selectButton.transform = CGAffineTransformMakeTranslation(0.0, -64.0);
    }
    else {
        self.selectButton.transform = CGAffineTransformMakeTranslation(0.0, 0.0);
    }
}

- (UIButton *)selectButton {
    if (!_selectButton) {
        _selectButton = [UIButton buttonWithType:UIButtonTypeCustom];
        CGFloat maxPadding = 10.0;
        CGFloat minPadding = 4.0;
        _selectButton.contentEdgeInsets = UIEdgeInsetsMake(maxPadding, maxPadding, minPadding, minPadding);
        [_selectButton setImage:[TFPhotoResourceHelper imageNamed:@"photo_unselected"] forState:UIControlStateNormal];
        [_selectButton setImage:[TFPhotoResourceHelper imageNamed:@"photo_selected"] forState:UIControlStateSelected];
        [_selectButton addTarget:self action:@selector(p_selectButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _selectButton;
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
