//
//  TFPhotoPickerLoadingView.h
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2017/1/18.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 图片选择加载view
 */
@interface TFPhotoPickerLoadingView : UIView

/**
 显示在某个view上

 @param view super view
 */
+ (nonnull instancetype)showInView:(nonnull UIView *)view;

/**
 消失
 */
- (void)dismiss;

@end
