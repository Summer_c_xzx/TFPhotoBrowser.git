//
//  TFPhotoPickerTakePhotoCell.h
//  TimeFaceDemoProject
//
//  Created by TFAppleWork-Summer on 2017/5/16.
//  Copyright © 2017年 Summer. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 图片选择，显示拍照的cell
 */
@interface TFPhotoPickerTakePhotoCell : UICollectionViewCell

@end
