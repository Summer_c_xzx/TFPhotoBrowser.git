//
//  TFICloudDownloadViewAnimationInfoObject.m
//  MyCoolAnimationCollectionProject
//
//  Created by TFAppleWork-Summer on 2017/2/6.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import "TFICloudDownloadViewAnimationInfoObject.h"

@implementation TFICloudDownloadViewAnimationInfoObject

- (instancetype)initWithFrame:(CGRect)frame cloudSize:(CGSize)cloudSize {
    self = [super init];
    if (self) {
        _frame = frame;
        CGFloat height = CGRectGetHeight(frame);
        CGFloat width = CGRectGetWidth(frame);
        CGFloat centerX = width*0.5;
        CGFloat centerY = height*0.5;
        CGFloat cloudWidth = cloudSize.width*0.6;
        CGFloat cloudLeft = (width-cloudWidth)*0.5;
        CGFloat leftRadius = cloudWidth*0.25;
        CGFloat rightRadius = cloudWidth*0.2;
        CGFloat midRadius = sqrtf(pow((cloudWidth-leftRadius-rightRadius)*0.5, 2)+pow(leftRadius-rightRadius, 2));
        CGFloat cloudHeight = 2*leftRadius + midRadius - (leftRadius - rightRadius);
        CGFloat cloudTop = (height-cloudHeight)*0.5;
        CGFloat beforeRadian = acosf((leftRadius-rightRadius)/midRadius);
        CGFloat cloudBottom = cloudTop+cloudHeight;
        CGPoint startPoint = CGPointMake(cloudLeft+leftRadius, cloudBottom);
        CGPoint leftCircleCenter = CGPointMake(cloudLeft+leftRadius, cloudBottom-leftRadius);
        CGPoint midCircleCenter = CGPointMake((cloudLeft*2+cloudWidth+leftRadius-rightRadius)*0.5, cloudBottom-leftRadius-rightRadius);
        CGPoint rightCircleCenter = CGPointMake(cloudLeft+cloudWidth-rightRadius, cloudBottom-rightRadius);
        _cloudPath = [UIBezierPath bezierPath];
        [_cloudPath moveToPoint:startPoint];
        [_cloudPath addArcWithCenter:leftCircleCenter radius:leftRadius startAngle:M_PI_2 endAngle:M_PI_2*3 clockwise:YES];
        [_cloudPath addArcWithCenter:midCircleCenter radius:midRadius startAngle:-M_PI_2-beforeRadian endAngle:-M_PI_2-beforeRadian+M_PI clockwise:YES];
        [_cloudPath addArcWithCenter:rightCircleCenter radius:rightRadius startAngle:-M_PI_2 endAngle:M_PI_2 clockwise:YES];
        [_cloudPath addLineToPoint:startPoint];
        
        CGFloat reloadArrowHeight = (leftRadius - rightRadius)*2;
        CGFloat reloadArrowWidth = reloadArrowHeight * cosf(M_PI_2/3.0);
        
        _reloadArrowPath = [UIBezierPath bezierPath];
        [_reloadArrowPath moveToPoint:CGPointMake(midCircleCenter.x, midCircleCenter.y-reloadArrowHeight*0.5)];
        [_reloadArrowPath addLineToPoint:CGPointMake(midCircleCenter.x+reloadArrowWidth, midCircleCenter.y)];
        [_reloadArrowPath addLineToPoint:CGPointMake(midCircleCenter.x, midCircleCenter.y+reloadArrowHeight*0.5)];
        
        CGFloat reloadCircleRadius = (cloudBottom - (leftRadius - rightRadius)*0.5 - midCircleCenter.y)*0.4;
        CGPoint reloadCircleCenter = CGPointMake(midCircleCenter.x, midCircleCenter.y+reloadCircleRadius);
        _reloadCirclePath = [UIBezierPath bezierPathWithArcCenter:reloadCircleCenter radius:reloadCircleRadius startAngle:M_PI_2*3 endAngle:(-M_PI_2+M_PI/2.0) clockwise:NO];
        
        CGFloat verticalLineHeight = cloudHeight * 0.5;
        CGFloat verticalLineTop = (height - verticalLineHeight)*0.5;
        CGFloat verticalLineBottom = verticalLineTop + verticalLineHeight;
        _verticalLineStartPath = [UIBezierPath bezierPath];
        [_verticalLineStartPath moveToPoint:CGPointMake(centerX, verticalLineTop)];
        [_verticalLineStartPath addLineToPoint:CGPointMake(centerX, verticalLineBottom)];
        
        _verticalLineEndPath = [UIBezierPath bezierPath];
        CGFloat vericalLineEndY = (height - cloudSize.height)*0.5;
        [_verticalLineEndPath moveToPoint:CGPointMake(centerX, vericalLineEndY)];
        [_verticalLineEndPath addLineToPoint:CGPointMake(centerX, vericalLineEndY)];
        
        CGFloat arrowWidth = cloudWidth * 0.3;
        CGFloat arrowLeft = (width - arrowWidth)*0.5;
        CGFloat arrowHeight = arrowWidth *  0.5;
        _arrowStartPath = [UIBezierPath bezierPath];
        [_arrowStartPath moveToPoint:CGPointMake(arrowLeft,verticalLineBottom - arrowHeight)];
        [_arrowStartPath addLineToPoint:CGPointMake(centerX, verticalLineBottom)];
        [_arrowStartPath addLineToPoint:CGPointMake(arrowLeft + arrowWidth, verticalLineBottom - arrowHeight)];
        
        CGFloat arrowBounceHeight = 2.0;
        _arrowBounceDownPath = [UIBezierPath bezierPath];
        [_arrowBounceDownPath moveToPoint:CGPointMake(arrowLeft, verticalLineBottom - arrowHeight)];
        [_arrowBounceDownPath addLineToPoint:CGPointMake(centerX, verticalLineBottom + arrowBounceHeight)];
        [_arrowBounceDownPath addLineToPoint:CGPointMake(arrowLeft + arrowWidth, verticalLineBottom - arrowHeight)];
        
        _arrowBounceUpPath = [UIBezierPath bezierPath];
        [_arrowBounceUpPath moveToPoint:CGPointMake(arrowLeft, verticalLineBottom - arrowHeight)];
        [_arrowBounceUpPath addLineToPoint:CGPointMake(centerX, centerY - arrowBounceHeight)];
        [_arrowBounceUpPath addLineToPoint:CGPointMake(arrowLeft + arrowWidth, verticalLineBottom - arrowHeight)];
        
        _arrowEndPath = [UIBezierPath bezierPath];
        [_arrowEndPath moveToPoint:CGPointMake(arrowLeft, centerY)];
        [_arrowEndPath addLineToPoint:CGPointMake(centerX, centerY)];
        [_arrowEndPath addLineToPoint:CGPointMake(arrowLeft + arrowWidth, centerY)];
        
        _arrowCloudStartPath = [UIBezierPath bezierPath];
        [_arrowCloudStartPath moveToPoint:startPoint];
        [_arrowCloudStartPath addLineToPoint:startPoint];
        
        CGFloat successWidth = cloudWidth * 0.5;
        CGFloat successHeight = cloudHeight * 0.6;
        CGFloat successLeft = (width - successWidth)*0.5;
        CGFloat successTop = (height- successHeight)*0.5;
        CGFloat successArrowHeight = successHeight * 0.4;
        CGFloat successArrowWidth = successWidth * 0.35;
        CGFloat successBottom = successTop + successHeight;
        _successPath = [UIBezierPath bezierPath];
        [_successPath moveToPoint:CGPointMake(successLeft,successBottom-successArrowHeight)];
        [_successPath addLineToPoint:CGPointMake(successLeft+successArrowWidth, successBottom)];
        [_successPath addLineToPoint:CGPointMake(successLeft+successWidth, successTop)];
        
    }
    return self;
}

- (CAKeyframeAnimation *)circleLoadingAnimation {
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation"];
    animation.values = @[@0.0,@(M_PI_2),@(M_PI),@(M_PI_2*3),@(M_PI*2)];
    animation.duration = 1.5;
    animation.repeatCount = CGFLOAT_MAX;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    return animation;
}

- (CABasicAnimation *)lineToPointAnimation {
    CABasicAnimation *lineUpAnimation = [CABasicAnimation animationWithKeyPath:@"path"];
    lineUpAnimation.fromValue = (__bridge id)_verticalLineStartPath.CGPath;
    lineUpAnimation.toValue = (__bridge id)_verticalLineEndPath.CGPath;
    lineUpAnimation.duration = 1.0;
    lineUpAnimation.repeatCount = 1.0;
    lineUpAnimation.beginTime = CACurrentMediaTime() + 0.60;
    lineUpAnimation.removedOnCompletion = NO;
    lineUpAnimation.fillMode = kCAFillModeForwards;
    lineUpAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    return lineUpAnimation;
}

- (CAKeyframeAnimation *)arrowToPointAnimation {
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"path"];
    animation.values = @[(__bridge id)_arrowStartPath.CGPath,
                         (__bridge id)_arrowBounceDownPath.CGPath,
                         (__bridge id)_arrowBounceUpPath.CGPath,
                         (__bridge id)_arrowEndPath.CGPath,
                         (__bridge id)_arrowCloudStartPath.CGPath,
                         ];
    animation.duration = 2.0;
    animation.repeatCount = 1.0;
    animation.removedOnCompletion = NO;
    animation.fillMode = kCAFillModeForwards;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    return animation;
}

- (CABasicAnimation *)cloudToSuccessAnimation {
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"path"];
    animation.fromValue = (__bridge id)_cloudPath.CGPath;
    animation.toValue = (__bridge id)_successPath.CGPath;
    animation.duration = 1.0;
    animation.repeatCount = 1.0;
    animation.removedOnCompletion = NO;
    animation.fillMode = kCAFillModeForwards;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    return animation;
}

- (CABasicAnimation *)successByCircleAnimation {
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    animation.fromValue = @0.0;
    animation.toValue = @1.0;
    animation.duration = 1.0;
    animation.repeatCount = 1.0;
    animation.removedOnCompletion = NO;
    animation.fillMode = kCAFillModeForwards;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    return animation;
}


@end
