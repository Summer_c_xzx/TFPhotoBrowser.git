//
//  TFPhotoPickerMomentModel.m
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/11/14.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#import "TFPhotoPickerMomentModel.h"
#import "TFPhotoResourceHelper.h"

@implementation TFPhotoPickerMomentModel

- (void)setDateStringWithDate:(NSDate *)date {
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *currentDate = [NSDate date];
    NSDateComponents *dateComponents = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:date];
    
    NSDateComponents *diffComponents = [calendar components:NSCalendarUnitDay|NSCalendarUnitYear fromDate:[calendar dateFromComponents:dateComponents] toDate:currentDate options:NSCalendarWrapComponents];

    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    if (diffComponents.year>0) {
        //不同年
        [formatter setDateFormat:[TFPhotoResourceHelper localizedStringWithKey:@"dateFormatterOverYear"]];
        _dateString = [formatter stringFromDate:date];
    }
    else if (diffComponents.day>6) {
        //大于一周
        [formatter setDateFormat:[TFPhotoResourceHelper localizedStringWithKey:@"dateFormatterOverWeek"]];
        _dateString = [formatter stringFromDate:date];
    }
    else if (diffComponents.day>1) {
        //一周内
        [formatter setDateFormat:[TFPhotoResourceHelper localizedStringWithKey:@"dateFormatterOverInWeek"]];
        _dateString = [formatter stringFromDate:date];
    }
    else if (diffComponents.day==1) {
        _dateString = [TFPhotoResourceHelper localizedStringWithKey:@"Yesterday"];
    }
    else if (diffComponents.day==0) {
        _dateString = [TFPhotoResourceHelper localizedStringWithKey:@"Today"];
    }
}

@end
