//
//  TFPhotoPickerItemModel.h
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/11/15.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>

@class TFPhotoPickerMomentModel;
@class TFPhotoPickerItemModel;

/**
 图片选择Item数据模型协议
 */
@protocol TFPhotoPickerItemModelDelegate <NSObject>

/**
 图片选择模型正在从iCloud上下载图片，此方法调用时已回归主线程

 @param model 数据模型
 @param progress 下载进度
 */
- (void)photoPickerItemModel:(nonnull TFPhotoPickerItemModel *)model downloadingImageFromICloudWithProgress:(float)progress;

/**
 图片选择模型完成从iCloud上下载图片

 @param model 数据模型
 @param image 下载的图片
 */
- (void)photoPickerItemModel:(nonnull TFPhotoPickerItemModel *)model finishDownloadImageFromICloud:(nonnull UIImage *)image;

/**
 图片选择模型从iCloud上下载图片失败

 @param model TFPhotoPickerItemModel
 */
- (void)photoPickerItemModelFailDownloadImageFromICloud:(nonnull TFPhotoPickerItemModel *)model;

@end

/**
 图片选择Item数据模型类型
 */
typedef NS_ENUM(NSUInteger, TFPhotoPickerItemModelType) {
    TFPhotoPickerItemModelTypeNormal = 1,///<正常情况
    TFPhotoPickerItemModelTypeDownloadingImageFromICloud,///<正在从iCloud下载图片
    TFPhotoPickerItemModelTypeDownloadImageFail,///<下载图片失败
};

/**
 图片选择Item数据模型
 */
@interface TFPhotoPickerItemModel : NSObject

/**
 图片asset
 */
@property (nonatomic, strong, nonnull) PHAsset *asset;

/**
 图片大小
 */
@property (nonatomic, assign) CGSize assetSize;

/**
 是否选中
 */
@property (nonatomic, assign) BOOL select;

/**
 图片是否在iCloud上
 */
@property (nonatomic, assign) BOOL shouldDownloadFromICloud;

/**
 相当于缓存机制，判断保存缩略图，若存在则不再获取
 */
@property (nonatomic, strong, nullable) UIImage *image;

/**
 时长描述
 */
@property (nonatomic, strong, nullable) NSString *duration;

/**
 对应的时刻数据模型，若不存在则为nil
 */
@property (nonatomic, weak, nullable) TFPhotoPickerMomentModel *momentModel;

/**
 数据模型类型
 */
@property (nonatomic, assign) TFPhotoPickerItemModelType modelType;

/**
 代理
 */
@property (nonatomic, weak, nullable) id<TFPhotoPickerItemModelDelegate> delegate;

/**
 转换时长
 */
- (void)convertAssetDuration;

/**
 是否需要从iCloud下载图片

 @return BOOL
 */
- (BOOL)shouldDownloadFromICloud;

/**
 开始从iCloud下载图片

 */
- (void)startDownloadImageFromICloud;

@end
