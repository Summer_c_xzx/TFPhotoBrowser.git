//
//  TFPhotoPickerAlbumModel.h
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/11/16.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Photos/PhotosTypes.h>

@class PHAsset;
@class PHFetchResult;

/**
 图片选择相册的数据模型
 */
@interface TFPhotoPickerAlbumModel : NSObject

@property (nonatomic, strong, nullable) UIImage *thumbnailImage;///<相当于缓存机制，判断保存缩略图，若存在则不再获取

@property (nonatomic, strong, nonnull) NSString *albumName;///<相册名称

@property (nonatomic, assign) NSInteger assetsCount;///相册中PHAsset数量

@property (nonatomic, strong, nonnull) PHFetchResult *assets;///<PHAsset结果集

@property (nonatomic, assign) PHAssetCollectionSubtype subType;///<相册类型

@end
