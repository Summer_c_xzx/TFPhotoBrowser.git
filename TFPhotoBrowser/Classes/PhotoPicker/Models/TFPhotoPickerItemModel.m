//
//  TFPhotoPickerItemModel.m
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/11/15.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#import "TFPhotoPickerItemModel.h"
#import "TFPhotoHandler.h"

@interface TFPhotoPickerItemModel ()

@property (nonatomic, assign) PHImageRequestID currentRequestID;

@property (nonatomic, assign) BOOL hasCheckedInICloud;

@end

@implementation TFPhotoPickerItemModel

- (void)convertAssetDuration {
    NSMutableArray *durationArray = [NSMutableArray array];
    NSInteger seconds = ceilf(self.asset.duration);
    if (seconds) {
        NSInteger second = seconds%60;
        NSInteger minute = seconds/60;
        NSInteger hour = minute/60;
        [durationArray addObject:[NSString stringWithFormat:@"%02zd",second]];
        if (hour) {
            minute = minute%60;
            [durationArray insertObject:[NSString stringWithFormat:@"%02zd",minute] atIndex:0];
            [durationArray insertObject:[NSString stringWithFormat:@"%02zd",hour] atIndex:0];
        }
        else {
            [durationArray insertObject:[NSString stringWithFormat:@"%02zd",minute] atIndex:0];
        }
        NSString *durationString = [durationArray componentsJoinedByString:@":"];
        self.duration = durationString;
    }
}

- (BOOL)shouldDownloadFromICloud {
    if (!_hasCheckedInICloud) {
        typeof(self) __weak weakSelf = self;
        __block BOOL shouldDownload = NO;
        [[TFPhotoHandler sharedHandler] checkImageIsInICloudWithAsset:self.asset resultHandler:^(UIImage *image, NSDictionary *info) {
            weakSelf.hasCheckedInICloud = YES;
            shouldDownload = [info[PHImageResultIsInCloudKey] boolValue];
        }];
        _shouldDownloadFromICloud = shouldDownload;
        return shouldDownload;
    }
    else {
        return _shouldDownloadFromICloud;
    }
}

- (void)startDownloadImageFromICloud {
    self.modelType = TFPhotoPickerItemModelTypeDownloadingImageFromICloud;
    typeof(self) __weak weakSelf = self;
   _currentRequestID = [[TFPhotoHandler sharedHandler] downLoadICloudImageWithAsset:self.asset progressHandler:^(double progress, NSError * _Nullable error, BOOL * _Nonnull stop, NSDictionary * _Nullable info) {
        if (!error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([weakSelf.delegate respondsToSelector:@selector(photoPickerItemModel:downloadingImageFromICloudWithProgress:)]) {
                    [weakSelf.delegate photoPickerItemModel:weakSelf downloadingImageFromICloudWithProgress:progress];
                }
            });
        }
        else {
            weakSelf.modelType = TFPhotoPickerItemModelTypeDownloadImageFail;
        }
    } resultHandler:^(UIImage *image, NSDictionary *info) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (image==nil) {
                weakSelf.modelType = TFPhotoPickerItemModelTypeDownloadImageFail;
                if ([weakSelf.delegate respondsToSelector:@selector(photoPickerItemModelFailDownloadImageFromICloud:)]) {
                    [weakSelf.delegate photoPickerItemModelFailDownloadImageFromICloud:weakSelf];
                }
            }
            else {
                weakSelf.modelType = TFPhotoPickerItemModelTypeNormal;
                weakSelf.shouldDownloadFromICloud = NO;
                if ([weakSelf.delegate respondsToSelector:@selector(photoPickerItemModel:finishDownloadImageFromICloud:)]) {
                    [weakSelf.delegate photoPickerItemModel:weakSelf finishDownloadImageFromICloud:image];
                }
            }
            
        });
        
    }];
}

- (void)dealloc {
    if (_modelType==TFPhotoPickerItemModelTypeDownloadingImageFromICloud) {
        [[TFPhotoHandler sharedHandler]cancelICloudDownloadWith:_currentRequestID];
    }
}

@end
