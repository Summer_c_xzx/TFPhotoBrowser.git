//
//  TFPhotoPickerMomentModel.h
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/11/14.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TFPhotoPickerItemModel.h"

/**
 图片选择日期模型
 */
@interface TFPhotoPickerMomentModel : NSObject

@property (nonatomic, strong, nonnull) NSString *dateString;///<日期

@property (nonatomic, strong, nullable) NSString *locationTitle;///<位置信息

@property (nonatomic, strong, nullable) NSString *locationDetail;///<位置详情

@property (nonatomic, strong, nonnull) NSMutableArray<TFPhotoPickerItemModel *> *items;///<每个item

@property (nonatomic, strong, nonnull) PHFetchResult *fetchResult;///<获取到的PHAsset结果集

@property (nonatomic, assign) NSInteger selectCount;///<选中的count

@property (nonatomic, strong, nonnull) NSString *localIdentifier;///<本地时刻相册标识符

/**
 设置日期

 @param date NSDate
 */
- (void)setDateStringWithDate:(nonnull NSDate *)date;

@end
