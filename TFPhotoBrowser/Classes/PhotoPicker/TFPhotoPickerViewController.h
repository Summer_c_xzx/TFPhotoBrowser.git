//
//  TFPhotoPickerViewController.h
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/11/11.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>
//图片浏览
#import "TFPhotoBrowserViewController.h"
#import "TFPhotoPickerPublicProtocol.h"

@class TFPhotoPickerViewController;
@class TFPhotoPickerItemModel;
@class TFPhotoPickerMomentModel;

/**
 图片选择代理
 */
@protocol TFPhotoPickerViewControllerDelegate <NSObject>

@optional;

/**
 图片选择数量已到达最大数量。默认会弹出一个系统的Alert.
 @param photoPickerViewController 图片选择器
 */
- (void)photoPickerViewControllerSelectCountIsOverMaxSelectCount:(nonnull TFPhotoPickerViewController *)photoPickerViewController;

/**
 图片选择器自定义底部view，若此代理不实现，则会默认创建一个bottomBar,
 @param photoPickerViewController 图片选择器
 @warning 会默认给bottomView添加距底部为0，左右边距为0的约束，自定义高度需要设置bottomView的frame中的高度，若为0默认为49.0
 @return UIView.
 */
- (nonnull UIView *)photoPickerViewControllerCustomBottomView:(nonnull TFPhotoPickerViewController *)photoPickerViewController;

/**
 底部工具栏已经创建之后，在此代理中修改bottomBar的item，等等

 @param photoPickerViewController 图片选择器
 @param bottomBar 底部工具栏
 @warning 默认左有一个拍照的item,右边记录当前选中数量的item会在selectMaxCount大于等于1时创建,中间有一个控制分割距离UIBarButtonSystemItemFlexibleSpace类型的item
 */
- (void)photoPickerViewController:(nonnull TFPhotoPickerViewController *)photoPickerViewController didCreatedBottomBar:(nonnull UIToolbar *)bottomBar;

/**
 自定义约束

 @param photoPickerViewController 图片选择器
 @param collectionView 图片显示的CollectionView
 @param bottomView 底部view
 */
- (void)photoPickerViewController:(nonnull TFPhotoPickerViewController *)photoPickerViewController customConstraintsWithSubView:(nonnull UICollectionView *)collectionView bottomView:(nonnull UIView *)bottomView;

/**
 底部工具栏照相功能模块

 @param photoPickerViewController 图片选择器
 */
- (void)photoPickerViewControllerBottomBarCameraItemAction:(nonnull TFPhotoPickerViewController *)photoPickerViewController;

/**
 图片选择完成时调用该方法

 @param photoPickerViewController 图片选择器
 @param selectedAssetsArray 选择的PHAsset数组
 */
- (void)photoPickerViewController:(nonnull TFPhotoPickerViewController *)photoPickerViewController didFinishPickWithSelectedAssetsArray:(nonnull NSMutableArray<PHAsset *> *)selectedAssetsArray;

/**
 图片选择器选择了某张图片时调用

 @param photoPickerViewController 图片选择器
 @param asset PHAsset
 */
- (void)photoPickerViewController:(nonnull TFPhotoPickerViewController *)photoPickerViewController didSelectAsset:(nonnull PHAsset *)asset;

/**
 图片选择器取消选择了某张图片时调用

 @param photoPickerViewController 图片选择器
 @param asset PHAsset
 */
- (void)photoPickerViewController:(nonnull TFPhotoPickerViewController *)photoPickerViewController didDeselectAsset:(nonnull PHAsset *)asset;

/**
 默认ItemCell刷新UI时调用

 @param photoPickerViewController 图片选择器
 @param cell UICollectionViewCell<TFPhotoPickerItemCollectionViewCellPublic>
 @param indexPath 索引
 */
- (void)photoPickerViewController:(nonnull TFPhotoPickerViewController *)photoPickerViewController didReloadCell:(nonnull UICollectionViewCell<TFPhotoPickerItemCollectionViewCellPublic> *)cell atIndexPath:(nonnull NSIndexPath *)indexPath;

/**
 自定义cell（需服从TFPhotoPickerItemCollectionViewCellPublic协议）

 @param photoPickerViewController 图片选择器
 @return Class
 */
- (nonnull Class)photoPickerViewControllerCellClassForItem:(nonnull TFPhotoPickerViewController *)photoPickerViewController;

/**
 自定义时刻相册headerView（需服务协议TFPhotoPickerCollectionViewSectionHeaderViewPublic）

 @param photoPickerViewController 图片选择器
 @return Class
 */
- (nonnull Class)photoPickerViewControllerClassForMomentAlbumSectionHeaderView:(nonnull TFPhotoPickerViewController *)photoPickerViewController;

/**
 自定义图片浏览器大图预览展示cell

 @param photoPickerViewController 图片选择器
 @warning cell的class必须是TFPhotoPickerPhotoBrowserDisplayCell的子类
 @return Class
 */
- (nonnull Class)photoPickerViewControllerClassForPhotoBrowserDisplayCell:(nonnull TFPhotoPickerViewController *)photoPickerViewController;

/**
 自定义图片浏览器大图预览展示视频cell
 
 @param photoPickerViewController 图片选择器
 @warning cell的class必须是TFPhotoPickerPhotoBrowserDisplayCell的子类
 @return Class
 */
- (nonnull Class)photoPickerViewControllerClassForPhotoBrowserVideoDisplayCell:(nonnull TFPhotoPickerViewController *)photoPickerViewController;

/**
 自定义图片选择器拍照cell

 @param photoPickerViewController 图片选择器
 @return Class
 */
- (nonnull Class)photoPickerViewControllerClassForTakePhotoCell:(nonnull TFPhotoPickerViewController *)photoPickerViewController;

/**
 默认SectionHeaderView刷新UI时调用

 @param photoPickerViewController 图片选择器
 @param headerView TFPhotoPickerCollectionViewSectionHeaderView
 */
- (void)photoPickerViewController:(nonnull TFPhotoPickerViewController *)photoPickerViewController didReloadHeaderView:(nonnull UICollectionReusableView<TFPhotoPickerCollectionViewSectionHeaderViewPublic> *)headerView;

@end

/**
 照片选择显示类型
 */
typedef NS_ENUM(NSUInteger, TFPhotoPickerStyle) {
    /**
     照片选择显示类型--时刻分组样式
     */
    TFPhotoPickerStyleMomentGroup,
    /**
     照片选择显示类型-Collection列表样式
     */
    TFPhotoPickerStyleCollectionList,
};

/**
 图片选择器
 */
@interface TFPhotoPickerViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,TFPhotoPickerItemCollectionViewCellDelegate,TFPhotoPickerCollectionViewSectionHeaderViewDelegate,TFPhotoBrowserViewControllerDataSource,TFPhotoBrowserViewControllerDelegate>

/**
 根据photoPicker样式创建图片选择器

 @param photoPickerStyle 样式
 @return TFPhotoPickerViewController
 */
- (nonnull instancetype)initWithStyle:(TFPhotoPickerStyle)photoPickerStyle;

/**
 代理
 */
@property (nonatomic, weak, nullable) id<TFPhotoPickerViewControllerDelegate> delegate;

/**
 显示的collectionView
 */
@property (nonatomic, strong, readonly, nonnull) UICollectionView *collectionView;

/**
 底部view
 */
@property (nonatomic, strong, readonly, nonnull) UIView *bottomView;

/**
 媒体类型,PHAssetMediaType枚举类型，默认只有PHAssetMediaTypeImage.
 */
@property (nonatomic, strong, nonnull) NSArray<NSNumber *> *mediaTypes;

/**
 默认选择的最多图片数量，默认为0不限制
 */
@property (nonatomic, assign) NSInteger selectMaxCount;

/**
 选中的PHAsset数组
 */
@property (nonatomic, strong, nonnull) NSMutableArray<PHAsset *> *selectAssetsArray;

/**
 选中的PHAsset的唯一标识符
 */
@property (nonatomic, strong, nonnull) NSMutableArray<NSString *> *selectPhotoIdentifiersArray;

/**
 图片选择器section header view class.

 @return Class
 */
- (nonnull Class)photoPickerSectionHeaderViewClass;

/**
 图片选择器 cell class.

 @return Class
 */
- (nonnull Class)photoPickerCellClass;

/**
 图片浏览器cell class.

 @return Class
 */
- (nonnull Class)photoBrowserDisplayCellClass;

/**
 图片浏览器视频cell class.

 @return Class
 */
- (nonnull Class)photoBrowserVideoDisplayCellClass;

/**
 图片选择器 cell class.

 @return Class
 */
- (nonnull Class)photoPickerTakePhotoCellClass;

/**
 给子view添加约束，子类可重写此方法定义自己的布局约束
 */
- (void)addConstraintsForSubViews;

/**
 获取默认的底部view，默认返回一个UIToolBar

 @return UIView
 */
- (nonnull UIView *)getDefaultBottomView;

/**
 获取默认授权失败view

 @return UIView
 */
- (nonnull UIView *)getDefaultAuthorizationStatusDeniedView;

/**
 获取默认没有图片时的view

 @return UIView
 */
- (nonnull UIView *)getDefaultNoPhotosView;


@end
