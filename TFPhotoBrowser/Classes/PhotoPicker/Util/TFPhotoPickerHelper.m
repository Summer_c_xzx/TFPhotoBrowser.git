//
//  TFPhotoPickerHelper.m
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/11/14.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#import "TFPhotoPickerHelper.h"
#import "TFPhotoHandler.h"

@implementation TFPhotoPickerHelper

+ (CGFloat)imageMinSpace {
    return 0.5;
}

+ (CGFloat)imagePortraitSize {
    static CGFloat imageProtraitSize = 0.0;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        CGFloat width = MIN(CGRectGetWidth([UIScreen mainScreen].bounds), CGRectGetHeight([UIScreen mainScreen].bounds));
        NSInteger itemCount = 4;
        imageProtraitSize = (width - (itemCount-1)*[TFPhotoPickerHelper imageMinSpace])/itemCount;
    });
    return imageProtraitSize;
}

+ (CGFloat)imageLandScapeSize {
    static CGFloat imageLandScapeSize = 0.0;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        CGFloat width = MAX(CGRectGetWidth([UIScreen mainScreen].bounds), CGRectGetHeight([UIScreen mainScreen].bounds));;
        NSInteger itemCount = 7;
        imageLandScapeSize = (width - (itemCount-1)*[TFPhotoPickerHelper imageMinSpace])/itemCount;
    });
    return imageLandScapeSize;
}

+ (CGFloat)imageShowSize {
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    CGFloat itemWidth = 0.0;
    switch (orientation) {
        case UIDeviceOrientationLandscapeLeft:
        case UIDeviceOrientationLandscapeRight:
        {
            //水平方向
            itemWidth = [TFPhotoPickerHelper imageLandScapeSize];
        }
            break;
        case UIDeviceOrientationPortrait:
        case UIDeviceOrientationPortraitUpsideDown:
        case UIDeviceOrientationFaceUp:
        case UIDeviceOrientationFaceDown:
        case UIDeviceOrientationUnknown:
        {
            //竖直方向
            itemWidth = [TFPhotoPickerHelper imagePortraitSize];
        }
            break;
        default:
            break;
    }
    return floorf(itemWidth);
}

+ (CGSize)imageSize {
    CGFloat imageWidth = floorf(MAX([TFPhotoPickerHelper imagePortraitSize], [TFPhotoPickerHelper imageLandScapeSize]) * [UIScreen mainScreen].scale);
    CGSize assetSize = CGSizeMake(imageWidth, imageWidth);
    return assetSize;
}

+ (NSMutableArray<TFPhotoPickerMomentModel *> *)getMomentsAlbumArrayWithMomentFetchResult:(PHFetchResult<PHAssetCollection *> *)momentFetchResult mediaTypes:(NSArray<NSNumber *> *)mediaTypes enumerateItemModels:(void (^)(TFPhotoPickerItemModel *))enumerateItemModels {
    //遍历数组
    NSMutableArray *momentsArr = [NSMutableArray array];
    for (PHAssetCollection *collection in momentFetchResult) {
        TFPhotoPickerMomentModel *momentModel = [TFPhotoPickerHelper createMomentModelWithPHAssetCollection:collection mediaTypes:mediaTypes enumerateItemModels:enumerateItemModels];
        if (momentModel) {
            [momentsArr addObject:momentModel];
        }
    }
    return momentsArr;
}

+ (NSMutableArray<TFPhotoPickerAlbumModel *> *)getAlbumListArrayWithMediaTypes:(NSArray<NSNumber *> *)mediaTypes insertMomentAlbumModel:(TFPhotoPickerAlbumModel *)momentAlbumModel selectedAlbumModel:(TFPhotoPickerAlbumModel *)selectedAlbumModel enumerateItemModels:(void (^)(TFPhotoPickerItemModel *))enumerateItemModels {
    
    NSMutableArray *albumListArray = [NSMutableArray array];
    //添加已选择的相册
    if (selectedAlbumModel) {
        [albumListArray addObject:selectedAlbumModel];
    }
    //添加时刻相册数据
    if (momentAlbumModel) {
        [albumListArray addObject:momentAlbumModel];
    }
    
    //添加其它相册
    [TFPhotoHandler enumerateAllValidPHAssetCollection:^(PHAssetCollection *collection) {
        //检测collection中是否有图片
        PHFetchResult *result = [TFPhotoHandler getAssetsWithCollection:collection mediaTypes:mediaTypes];
        if (result.count) {
            TFPhotoPickerAlbumModel *model = [[TFPhotoPickerAlbumModel alloc] init];
            model.albumName = collection.localizedTitle;
            model.assetsCount = result.count;
            model.assets = result;
            model.subType = collection.assetCollectionSubtype;
            [albumListArray addObject:model];
            //判断相册是否分享的类型,若是分享类型需要创建新的itemModel，分享类型的图片和本地图片的localIdentifier不同
            if (collection.assetCollectionSubtype == PHAssetCollectionSubtypeAlbumCloudShared | collection.assetCollectionSubtype == PHAssetCollectionSubtypeAlbumMyPhotoStream) {
                for (PHAsset *asset in result) {
                    TFPhotoPickerItemModel *itemModel = [TFPhotoPickerHelper createItemModelWithAsset:asset momentModel:nil];
                    if (enumerateItemModels) {
                        enumerateItemModels(itemModel);
                    }
                }
            }
        }
    }];
    
    return albumListArray;
}

+ (TFPhotoPickerMomentModel *)createMomentModelWithPHAssetCollection:(PHAssetCollection *)assetCollection mediaTypes:(NSArray<NSNumber *> *)mediaTypes enumerateItemModels:(void (^)(TFPhotoPickerItemModel *))enumerateItemModels {
    //获取图片数组
    PHFetchResult *assetsResult = [TFPhotoHandler getAssetsWithCollection:assetCollection mediaTypes:mediaTypes];
    if (assetsResult.count) {
        TFPhotoPickerMomentModel *momentModel = [[TFPhotoPickerMomentModel alloc] init];
        [momentModel setDateStringWithDate:assetCollection.startDate];
        momentModel.locationTitle = assetCollection.localizedTitle;
        if (assetCollection.localizedLocationNames.count) {
            momentModel.locationDetail = [assetCollection.localizedLocationNames componentsJoinedByString:@" "];
        }
        momentModel.localIdentifier = assetCollection.localIdentifier;
        momentModel.fetchResult = assetsResult;
        NSMutableArray *itemsArr = [NSMutableArray array];
        for (PHAsset *asset in assetsResult) {
            TFPhotoPickerItemModel *itemModel = [TFPhotoPickerHelper createItemModelWithAsset:asset momentModel:momentModel];
            [itemsArr addObject:itemModel];
            if (enumerateItemModels) {
                enumerateItemModels(itemModel);
            }
        }
        momentModel.items = itemsArr;
        return momentModel;
    }
    return nil;
}

+ (TFPhotoPickerItemModel *)createItemModelWithAsset:(PHAsset *)asset momentModel:(TFPhotoPickerMomentModel *)momentModel {
    TFPhotoPickerItemModel *itemModel = [[TFPhotoPickerItemModel alloc] init];
    itemModel.modelType = TFPhotoPickerItemModelTypeNormal;
    itemModel.asset = asset;
    itemModel.assetSize = [TFPhotoPickerHelper imageSize];
    itemModel.momentModel = momentModel;
    [itemModel convertAssetDuration];
    return itemModel;
}


@end
