//
//  TFPhotoPickerHelper.h
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/11/14.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TFPhotoPickerMomentModel.h"
#import "TFPhotoPickerAlbumModel.h"
#import "TFPhotoPickerItemModel.h"

/**
 图片选择工具类
 */
@interface TFPhotoPickerHelper : NSObject

/**
 图片最小间距
 */
+ (CGFloat)imageMinSpace;

/**
 获取当前方向图片显示大小

 @return 图片大小
 */
+ (CGFloat)imageShowSize;

/**
 图片的实际大小

 @return CGSize
 */
+ (CGSize)imageSize;

/**
 获取时刻相册数组

 @param mediaTypes 媒体类型
 @param enumerateItemModels 遍历所有的itemModel
 @return NSArray.TFPhotoPickerMomentModel数组
 */
+ (nonnull NSMutableArray<TFPhotoPickerMomentModel *> *)getMomentsAlbumArrayWithMomentFetchResult:(nonnull PHFetchResult<PHAssetCollection *> *)momentFetchResult mediaTypes:(nonnull NSArray<NSNumber *> *) mediaTypes enumerateItemModels:(nonnull void(^)(TFPhotoPickerItemModel * __nonnull itemModel))enumerateItemModels;

/**
 获取相册列表数组

 @param mediaTypes 媒体类型
 @param momentAlbumModel 时刻相册
 @param selectedAlbumModel 已选择相册
 @param enumerateItemModels 遍历所有的ItemModel
 @return NSArray.
 */
+ (nonnull NSMutableArray<TFPhotoPickerAlbumModel *> *)getAlbumListArrayWithMediaTypes:(nonnull NSArray<NSNumber *> *) mediaTypes insertMomentAlbumModel:(nonnull TFPhotoPickerAlbumModel *)momentAlbumModel selectedAlbumModel:(nonnull TFPhotoPickerAlbumModel *)selectedAlbumModel enumerateItemModels:(nullable void (^)(TFPhotoPickerItemModel * __nonnull itemModel))enumerateItemModels;

/**
 创建时刻相册模型

 @param assetCollection 相册结合
 @return TFPhotoPickerMomentModel
 */
+ (nonnull TFPhotoPickerMomentModel *)createMomentModelWithPHAssetCollection:(nonnull PHAssetCollection *)assetCollection  mediaTypes:(nonnull NSArray<NSNumber *> *) mediaTypes enumerateItemModels:(nullable void(^)(TFPhotoPickerItemModel * __nonnull itemModel))enumerateItemModels;

/**
 创建图片数据模型

 @param asset 图片PHAsset
 @param momentModel 对应的时刻模型
 @return TFPhotoPickerItemModel
 */
+ (nonnull TFPhotoPickerItemModel *)createItemModelWithAsset:(nonnull PHAsset *)asset momentModel:(nullable TFPhotoPickerMomentModel *)momentModel;

@end
