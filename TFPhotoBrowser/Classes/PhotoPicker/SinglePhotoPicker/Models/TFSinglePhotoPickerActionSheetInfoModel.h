//
//  TFSinglePhotoPickerActionSheetInfoModel.h
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/12/16.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 单选图片弹出的ActionSheet的信息数组
 */
@interface TFSinglePhotoPickerActionSheetInfoModel : NSObject

/**
 显示的文字信息
 */
@property (nonatomic, strong) NSString *info;

/**
 source type
 */
@property (nonatomic, assign) UIImagePickerControllerSourceType sourceType;

@end
