//
//  TFSinglePhotoPickerHelper.h
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/12/16.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TFSinglePhotoPickerActionSheetInfoModel.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "TFPhotoCropInfoModel.h"

@class TFSinglePhotoPickerHelper;
@class PHAsset;

/**
 图片单选代理
 */
@protocol TFSinglePhotoPickerHelperDelegate <NSObject>

@optional;

/**
 图片单选将要显示系统单选图片选择器调用

 @param photoPickerHelper 图片选择工具
 @param picker 图片单选
 */
- (void)singlePhotoPickerHelper:(nonnull TFSinglePhotoPickerHelper *)photoPickerHelper willPresentPicker:(nonnull UIImagePickerController *)picker;

/**
 图片单选,系统单选选择器取消时调用
 
 @warning 若实现该代理，则picker的dismiss需要自己调用
 @param photoPickerHelper 图片选择工具
 @param picker 图片单选
 */
- (void)singlePhotoPickerHelper:(nonnull TFSinglePhotoPickerHelper *)photoPickerHelper pickerDidCancel:(nonnull UIImagePickerController *)picker;

/**
 图片单选,系统单选选择器完成时调用
 
 @warning 若实现该代理，则picker的dismiss需要自己调用
 @param photoPickerHelper 图片选择工具
 @param picker 图片单选
 @param info 信息
 */
- (void)singlePhotoPickerHelper:(nonnull TFSinglePhotoPickerHelper *)photoPickerHelper picker:(nonnull UIImagePickerController *)picker didFinishPickingMediaWithInfo:(nonnull NSDictionary<NSString *,id> *)info;

/**
 图片单选，系统单选选择器将要显示某个VC时调用

 @param photoPickerHelper 图片选择工具
 @param picker 图片单选
 @param viewController vc
 */
- (void)singlePhotoPickerHelper:(nonnull TFSinglePhotoPickerHelper *)photoPickerHelper picker:(nonnull UIImagePickerController *)picker willShowViewController:(nonnull UIViewController *)viewController;

/**
 图片单选，系统单选选择器显示某个VC时调用

 @param photoPickerHelper 图片选择工具
 @param picker 图片单选
 @param viewController vc
 */
- (void)singlePhotoPickerHelper:(nonnull TFSinglePhotoPickerHelper *)photoPickerHelper picker:(nonnull UIImagePickerController *)picker didShowViewController:(nonnull UIViewController *)viewController;

/**
 图片单选自定义剪裁框

 @param photoPickerHelper 图片选择工具
 @param picker 图片单选
 */
- (nonnull TFPhotoCropInfoModel *)singlePhotoPickerHelper:(nonnull TFSinglePhotoPickerHelper *)photoPickerHelper pickerCustomCropInfoModel:(nonnull UIImagePickerController *)picker;

@end

@interface TFSinglePhotoPickerHelper : NSObject

/**
 当前的图片选择器
 */
@property (nonatomic, weak, nullable) UIImagePickerController *imagePickerViewController;

#pragma mark - public

/**
 通过ActionSheet方式选择单选图片

 @param viewController 被弹出的ViewController
 @param title ActionSheet title
 @param infos ActionSheet按钮数组，info 为按钮标题，infoType为对应的功能，若传nil，则默认拍照和从手机相册选取两种方式，可在TFPhotoDefaultConfig中统一修改
 @param mediaTypes 支持媒体类型数组，图片为(NSString *)kUTTypeImage，视频为(NSString *)kUTTypeMovie,若为nil，则默认只有图片类型.
 @param allowsEditing 是否允许编辑
 @param delegate 代理
 */
+ (void)showSystemSinglePhotoPickerActionSheetInViewController:(nonnull UIViewController *)viewController
                                                     withTitle:(nullable NSString *)title
                                                         infos:(nullable NSArray <TFSinglePhotoPickerActionSheetInfoModel *> *)infos
                                                    mediaTypes:(nullable NSArray<NSString *> *)mediaTypes
                                                 allowsEditing:(BOOL)allowsEditing
                                                      delegate:(nullable id<TFSinglePhotoPickerHelperDelegate>)delegate;

/**
 直接弹出系统的图片选择

 @param viewController 被弹出的ViewController
 @param sourceType 资源类型
 @param mediaTypes 支持媒体类型数组，图片为(NSString *)kUTTypeImage，视频为(NSString *)kUTTypeMovie,若为nil，则默认只有图片类型.
 @param allowsEditing 是否允许编辑
 @param delegate 代理
 */
+ (void)showSystemSinglePhotoPickerInViewController:(nonnull UIViewController *)viewController
                                       withSourceType:(UIImagePickerControllerSourceType)sourceType
                                         mediaTypes:(nullable NSArray<NSString *> *)mediaTypes
                                        allowsEditing:(BOOL)allowsEditing
                                           delegate:(nullable id<TFSinglePhotoPickerHelperDelegate>)delegate;

+ (void)showSystemSinglePhotoPickerInViewController:(nonnull UIViewController *)viewController
                                    customPickerControllerClass:(Class)customPickerControllerClass
                                     withSourceType:(UIImagePickerControllerSourceType)sourceType
                                         mediaTypes:(nullable NSArray<NSString *> *)mediaTypes
                                      allowsEditing:(BOOL)allowsEditing
                                           delegate:(nullable id<TFSinglePhotoPickerHelperDelegate>)delegate;


#pragma mark - help actions.

/**
 获取原图
 
 @param info 信息字典
 @return UIImage
 */
+ (nonnull UIImage *)getOriginalImageWithInfo:(nonnull NSDictionary<NSString *,id> *)info;

/**
 获取原图的PHAsset
 
 @param info 信息字典
 @return PHAsset
 */
+ (nonnull PHAsset *)getOriginalImageAssetWithInfo:(nonnull NSDictionary<NSString *,id> *)info;

/**
 获取剪裁之后的图片

 @param info 信息字典
 @return UIImage
 */
+ (nonnull UIImage *)getEditedImageWithInfo:(nonnull NSDictionary<NSString *,id> *)info;

@end
