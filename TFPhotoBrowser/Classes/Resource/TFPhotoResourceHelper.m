//
//  TFPhotoResourceHelper.m
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/11/14.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#import "TFPhotoResourceHelper.h"

@implementation TFPhotoResourceHelper

+ (UIImage *)imageNamed:(NSString *)imageName {
    
    NSString *imageBundleName = [NSString stringWithFormat:@"%@/Images/%@",[TFPhotoResourceHelper fileBundle].bundlePath,imageName];
    return [UIImage imageNamed:imageBundleName];
}

+ (NSString *)localizedStringWithKey:(NSString *)key {
    return [[TFPhotoResourceHelper fileBundle] localizedStringForKey:key value:@"" table:@"TFPhotoLocalString"];
}

+ (NSBundle *)fileBundle {
    static NSBundle *fileBundle = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSBundle *photoMainBundle = [NSBundle bundleForClass:[TFPhotoResourceHelper class]];
        fileBundle = [NSBundle bundleWithURL:[photoMainBundle URLForResource:@"TFPhotoResource" withExtension:@"bundle"]];
    });
    return fileBundle;
}

@end
