//
//  TFPhotoResourceHelper.h
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/11/14.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 资源管理工具类
 */
@interface TFPhotoResourceHelper : NSObject

/**
 获取Resource.boundle中的图片资源

 @param imageName 图片名称
 @return UIImage
 */
+ (nonnull UIImage *)imageNamed:(nonnull NSString *)imageName;

/**
 获取本地格式化字符串

 @param key 格式化字符串key
 @return NSString
 */
+ (nonnull NSString *)localizedStringWithKey:(nonnull NSString *)key;

@end
