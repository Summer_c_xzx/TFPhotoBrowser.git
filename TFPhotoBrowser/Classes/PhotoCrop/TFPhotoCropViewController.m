//
//  TFPhotoCropViewController.m
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2017/2/21.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import "TFPhotoCropViewController.h"
#import "TFPhotoCropView.h"
#import "TFPhotoResourceHelper.h"
#import "UIImage+TFPhoto.h"

@interface TFPhotoCropViewController ()

@property (nonatomic, strong) TFPhotoCropView *cropView;

@end

@implementation TFPhotoCropViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = [UIColor blackColor];
    [self.view addSubview:self.cropView];
    
    //添加底部view
    CGFloat bottomViewHeight = 60.0;
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.view.frame)-bottomViewHeight, CGRectGetWidth(self.view.frame), bottomViewHeight)];
    bottomView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
    CGFloat buttonInset = 10.0;
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setContentEdgeInsets:UIEdgeInsetsMake(buttonInset, buttonInset, buttonInset, buttonInset)];
    [cancelButton setTitle:[TFPhotoResourceHelper localizedStringWithKey:@"取消"] forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    cancelButton.titleLabel.font = [UIFont systemFontOfSize:16];
    [cancelButton addTarget:self action:@selector(p_bottomViewCancelButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:cancelButton];
    
    UIButton *pickerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [pickerButton setContentEdgeInsets:UIEdgeInsetsMake(buttonInset, buttonInset, buttonInset, buttonInset)];
    [pickerButton setTitle:[TFPhotoResourceHelper localizedStringWithKey:@"选取"] forState:UIControlStateNormal];
    [pickerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    pickerButton.titleLabel.font = [UIFont systemFontOfSize:16];
    [pickerButton addTarget:self action:@selector(p_bottomViewPickerButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:pickerButton];
    
    CGFloat buttonSpace = 5.0;
    
    cancelButton.translatesAutoresizingMaskIntoConstraints = NO;
    pickerButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    [NSLayoutConstraint constraintWithItem:cancelButton attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:bottomView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:buttonSpace].active = YES;
    [NSLayoutConstraint constraintWithItem:cancelButton attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:bottomView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0].active = YES;
    
    [NSLayoutConstraint constraintWithItem:pickerButton attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:bottomView attribute:NSLayoutAttributeRight multiplier:1.0 constant:-buttonSpace].active = YES;
    [NSLayoutConstraint constraintWithItem:pickerButton attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:bottomView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0].active = YES;
    
    [self.view addSubview:bottomView];
}

#pragma mark - lazy load.

- (void)p_bottomViewCancelButtonAction {
    if ([self.delegate respondsToSelector:@selector(photoCropViewControllerDidCancel:)]) {
        [self.delegate photoCropViewControllerDidCancel:self];
    }
    else {
        if (self.presentingViewController) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

- (void)p_bottomViewPickerButtonAction {
    self.cropedImage = [self.cropView clipImage];
    if ([self.delegate respondsToSelector:@selector(photoCropViewControllerDidFinish:)]) {
        [self.delegate photoCropViewControllerDidFinish:self];
    }
    else {
        [self p_bottomViewCancelButtonAction];
    }
}

- (TFPhotoCropView *)cropView {
    if (!_cropView) {
        _cropView = [[TFPhotoCropView alloc] initWithFrame:self.view.bounds];
        _cropView.image = [self.image tfPhoto_fixedOrientationImage];
        BOOL hasContentModel = [self.delegate respondsToSelector:@selector(photoCropViewControllerCropInfoModel:)];
        if (hasContentModel) {
            TFPhotoCropInfoModel *infoModel = [self.delegate photoCropViewControllerCropInfoModel:self];
            UIView *borderView = infoModel.borderView;
            UIBezierPath *path = infoModel.path;
            if (!infoModel.borderView) {
                borderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, infoModel.size.width, infoModel.size.height)];
                borderView.center = self.view.center;
                borderView.layer.cornerRadius = infoModel.cornerRadius;
                borderView.layer.borderWidth = infoModel.borderWidth;
                UIColor *borderColor = infoModel.borderColor;
                if (!borderColor) {
                    borderColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.85];
                }
                borderView.layer.borderColor = borderColor.CGColor;
                if (!infoModel.path) {
                    switch (infoModel.shapeType) {
                        case TFPhotoCropShapeTypeRect:
                        {
                            path = [UIBezierPath bezierPathWithRect:borderView.frame];
                        }
                            break;
                        case TFPhotoCropShapeTypeRoundRect: {
                            path = [UIBezierPath bezierPathWithRoundedRect:borderView.frame cornerRadius:infoModel.cornerRadius];
                        }
                            break;
                        case TFPhotoCropShapeTypeCircle: {
                            path = [UIBezierPath bezierPathWithArcCenter:borderView.center radius:infoModel.size.height*0.5 startAngle:0 endAngle:M_PI*2 clockwise:YES];
                        }
                            break;
                        default:
                            break;
                    }
                }
            }
            
            [_cropView addCropWithDecorateView:borderView transparentPath:path];
        }
        else {
            NSAssert(hasContentModel, @"must implement the delegate to give the crop border view add path.");
        }
    }
    return _cropView;
}

- (void)dealloc {
    NSLog(@"\n******************************************\n %s---line:%d \n******************************************",__func__,__LINE__);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
