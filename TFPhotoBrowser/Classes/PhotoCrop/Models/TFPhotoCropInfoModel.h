//
//  TFPhotoCropInfoModel.h
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2017/3/2.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 剪裁图形类型
 */
typedef NS_ENUM(NSUInteger, TFPhotoCropShapeType) {
    /**
     剪裁图形类型--矩形
     */
    TFPhotoCropShapeTypeRect,
    /**
     剪裁图形类型--圆角矩形
     */
    TFPhotoCropShapeTypeRoundRect,
    /**
     剪裁图形类型--圆形
     */
    TFPhotoCropShapeTypeCircle,
};

/**
 图片剪裁配置数据模型
 */
@interface TFPhotoCropInfoModel : NSObject

//---------矩形、圆角矩形、圆的剪裁框的快速实现，只需要赋值图形类型和剪裁大小以及对应的辅助属性----------//

/**
 图形类型，现支持三种矩形（rect），RoundRect（圆角矩形，需设置cornerRadius），Circle（圆形--半径为size.height的一半）
 */
@property (nonatomic, assign) TFPhotoCropShapeType shapeType;

/**
 剪裁框view边框宽度
 */
@property (nonatomic, assign) CGFloat borderWidth;

/**
 剪裁框view边框颜色,默认白色透明度0.6
 */
@property (nonatomic, strong, nullable) UIColor *borderColor;
/**
 若为圆角矩形的圆角半径
 */
@property (nonatomic, assign) CGFloat cornerRadius;

/**
 剪裁框的大小
 */
@property (nonatomic, assign) CGSize size;

//---------若需要完全自定义请实现以下属性，以自定义优先，borderView必须赋值才可自定义----------//

/**
 剪裁路径
 */
@property (nonatomic, strong, nullable) UIBezierPath *path;

/**
 自定义边框view
 */
@property (nonatomic, strong, nullable) UIView *borderView;

@end
