//
//  TFPhotoCropView.h
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2017/3/2.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 图片剪裁view
 */
@interface TFPhotoCropView : UIView

/**
 添加剪裁透明矩形框

 @param decorateView 透明框上的修饰view
 @param transparentRect 透明矩形
 */
- (void)addCropWithDecorateView:(nonnull UIView *)decorateView transparentRect:(CGRect)transparentRect;

/**
 添加剪裁透明矩形圆角框

 @param decorateView 透明框上的修饰view
 @param rect 透明矩形
 @param cornerRadius 圆角半径
 */
- (void)addCropWithDecorateView:(nonnull UIView *)decorateView transparentRoundRect:(CGRect)rect cornerRadius:(CGFloat)cornerRadius;

/**
 添加剪裁透明圆形框

 @param decorateView 透明框上的修饰view
 @param center 圆形中心点
 @param radius 圆形半径
 */
- (void)addCropWithDecorateView:(nonnull UIView *)decorateView transparentCircleCenter:(CGPoint)center radius:(CGFloat)radius;

/**
 添加剪裁自定义路径透明框

 @param decorateView 透明框上的修饰view
 @param path 自定义路径
 */
- (void)addCropWithDecorateView:(nonnull UIView *)decorateView transparentPath:(nonnull UIBezierPath *)path;

/**
 剪裁图片并获取对应的图片

 @return UIImage 剪裁后的图片
 */
- (nonnull UIImage *)clipImage;

/**
 图片
 */
@property (nonatomic, strong, nonnull) UIImage *image;

/**
 更新剪裁的图片

 @param image 图片
 */
- (void)updateImageWithImage:(nonnull UIImage *)image;


@end
