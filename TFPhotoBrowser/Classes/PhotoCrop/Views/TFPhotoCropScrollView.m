//
//  TFPhotoCropScrollView.m
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2017/2/21.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import "TFPhotoCropScrollView.h"

@interface TFPhotoCropScrollView ()<UIScrollViewDelegate>

@property (nonatomic, strong, readwrite) TFPhotoCropContentView *contentView;

@end

@implementation TFPhotoCropScrollView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.delegate = self;
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
        self.clipsToBounds = NO;
        [self addSubview:self.contentView];
    }
    return self;
}

#pragma mark - lazy load.

- (void)setImage:(UIImage *)image {
    _image = image;
    _contentView.transform = CGAffineTransformIdentity;
    _contentView.frame = self.bounds;
    _contentView.image = image;
}

- (TFPhotoCropContentView *)contentView {
    if (!_contentView) {
        _contentView = [[TFPhotoCropContentView alloc] init];
    }
    return _contentView;
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.contentView;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
