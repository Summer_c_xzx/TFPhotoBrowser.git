//
//  TFPhotoCropContentView.m
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2017/2/21.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import "TFPhotoCropContentView.h"

@interface TFPhotoCropContentView ()

@property (nonatomic, strong, readwrite) UIImageView *imageView;

@end

@implementation TFPhotoCropContentView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.imageView];
    }
    return self;
}

- (void)setImage:(UIImage *)image {
    _image = image;
    self.imageView.image = image;
    self.imageView.frame = self.bounds;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.imageView.frame = self.bounds;
}

#pragma mark - lazy load.

- (UIImageView *)imageView {
    if (!_imageView) {
        _imageView = [[UIImageView alloc] initWithFrame:self.bounds];
        _imageView.userInteractionEnabled = YES;
    }
    return _imageView;
}


@end
