//
//  TFPhotoCropView.m
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2017/3/2.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import "TFPhotoCropView.h"
#import "TFPhotoCropScrollView.h"

@interface TFPhotoCropView ()

@property (nonatomic, strong) UIBezierPath *cropPath;

@property (nonatomic, strong) TFPhotoCropScrollView *photoCropScrollView;

@property (nonatomic, strong) UIView *cropBackView;

@property (nonatomic, strong) UIView *decorateView;

@end

@implementation TFPhotoCropView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.photoCropScrollView];
        [self addSubview:self.cropBackView];
    }
    return self;
}

#pragma mark - public

- (void)addCropWithDecorateView:(UIView *)decorateView transparentRect:(CGRect)transparentRect {
    [self addCropWithDecorateView:decorateView transparentPath:[UIBezierPath bezierPathWithRect:transparentRect]];
}

- (void)addCropWithDecorateView:(UIView *)decorateView transparentRoundRect:(CGRect)rect cornerRadius:(CGFloat)cornerRadius {
    [self addCropWithDecorateView:decorateView transparentPath:[UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:cornerRadius]];
}

- (void)addCropWithDecorateView:(UIView *)decorateView transparentCircleCenter:(CGPoint)center radius:(CGFloat)radius {
    [self addCropWithDecorateView:decorateView transparentPath:[UIBezierPath bezierPathWithArcCenter:center radius:radius startAngle:0 endAngle:M_PI*2 clockwise:YES]];
}

- (void)addCropWithDecorateView:(UIView *)decorateView transparentPath:(UIBezierPath *)path {
    if (_decorateView.superview) {
        [_decorateView removeFromSuperview];
    }
    _decorateView = decorateView;
    
    [self addSubview:_decorateView];
    //添加透明路径
    _cropPath = path;
    CAShapeLayer *cropLayer = [CAShapeLayer layer];
    UIBezierPath *cropLayerPath = [UIBezierPath bezierPathWithRect:self.bounds];
    [cropLayerPath appendPath:[_cropPath bezierPathByReversingPath]];
    cropLayer.path = cropLayerPath.CGPath;
    self.cropBackView.layer.mask = cropLayer;
    [self p_layoutSubviews];
}

- (void)updateImageWithImage:(UIImage *)image {
    self.image = image;
    [self p_layoutSubviews];
}

- (void)p_layoutSubviews {

    
    CGRect cropFrame = CGPathGetBoundingBox(_cropPath.CGPath);
    //(origin = (x = 37.5, y = 253.5), size = (width = 300, height = 160))
    
    //计算scrollViewFrame
    CGFloat viewWidth = CGRectGetWidth(self.frame); //375
    CGFloat viewHeight = CGRectGetHeight(self.frame); //667
    CGFloat cropXScale = CGRectGetWidth(cropFrame)/self.image.size.width; //300 / 360
    CGFloat cropYScale = CGRectGetHeight(cropFrame)/self.image.size.height; //160 / 640
    CGFloat cropXMaxScale = self.image.size.width/CGRectGetWidth(cropFrame); // 360 / 300
    CGFloat cropYMaxScale = self.image.size.height/CGRectGetHeight(cropFrame); // 640 / 160
    CGFloat minScale = MIN(1.0, MAX(cropXScale, cropYScale));
    CGFloat maxScale = MAX(1.0, MAX(cropXMaxScale, cropYMaxScale));
    CGSize scrollViewSize = CGSizeMake(minScale*self.image.size.width, minScale*self.image.size.height);
    self.photoCropScrollView.contentInset = UIEdgeInsetsZero;
    self.photoCropScrollView.contentOffset = CGPointZero;
    self.photoCropScrollView.zoomScale = 1.0;
    self.photoCropScrollView.frame = CGRectMake((viewWidth-scrollViewSize.width)*0.5, (viewHeight-scrollViewSize.height)*0.5, scrollViewSize.width, scrollViewSize.height);
    self.photoCropScrollView.contentSize = scrollViewSize;
    self.photoCropScrollView.minimumZoomScale = 1.0;
    self.photoCropScrollView.maximumZoomScale = maxScale;
    
    
    CGFloat currentScale = MIN(viewWidth/scrollViewSize.width, viewHeight/scrollViewSize.height);
    self.photoCropScrollView.image = self.image;
    self.photoCropScrollView.zoomScale = currentScale;
    //设置当前的缩放比
    //添加contentInsets 上拉下拉的时候回到位置
    CGFloat hInset = MAX(0, (scrollViewSize.width - cropFrame.size.width)*0.5);
    CGFloat vInset = MAX(0, (scrollViewSize.height - cropFrame.size.height)*0.5);
    self.photoCropScrollView.contentInset = UIEdgeInsetsMake(vInset, hInset, vInset, hInset);
    [self.photoCropScrollView layoutIfNeeded];
}

- (UIImage *)clipImage {
    //获取剪裁路径的frame
    CGRect cropFrame = CGPathGetBoundingBox(_cropPath.CGPath);
    //获取当前缩放比
    CGFloat currentScale = self.image.size.width/CGRectGetWidth(self.photoCropScrollView.contentView.frame);
    //获取修正之后的剪裁的frame
    CGRect cropImageFrame = [self convertRect:cropFrame toView:self.photoCropScrollView.contentView];
    CGRect fixedCropImageFrame = [self p_fixCropImageFrame:cropImageFrame currentScale:currentScale];
    //修复剪裁路径大小和位置
    CGAffineTransform pathScaleTransform = CGAffineTransformMakeScale(currentScale, currentScale);
    CGPathRef scalePath = CGPathCreateCopyByTransformingPath(_cropPath.CGPath, &pathScaleTransform);
    CGRect scaledPathRect = CGPathGetBoundingBox(scalePath);
    CGAffineTransform pathTranslationTransForm = CGAffineTransformMakeTranslation(-CGRectGetMinX(scaledPathRect), -CGRectGetMinY(scaledPathRect));
    CGPathRef fixedPath = CGPathCreateCopyByTransformingPath(scalePath, &pathTranslationTransForm);
    //开始剪裁图片
    CGImageRef imgRef = CGImageCreateWithImageInRect(self.image.CGImage, fixedCropImageFrame);
    UIGraphicsBeginImageContext(fixedCropImageFrame.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0, fixedCropImageFrame.size.height);
    CGContextScaleCTM(context, 1, -1);
    CGContextAddPath(context, fixedPath);
    CGContextClosePath(context);
    CGContextClip(context);
    CGContextDrawImage(context, CGRectMake(0, 0, fixedCropImageFrame.size.width, fixedCropImageFrame.size.height), imgRef);
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    CGImageRelease(imgRef);
    UIGraphicsEndImageContext();
    return newImage;
}

- (CGRect)p_fixCropImageFrame:(CGRect)cropImageFrame currentScale:(CGFloat)currentScale {
    //回正位置
    CGFloat width = CGRectGetWidth(cropImageFrame);
    CGFloat height = CGRectGetHeight(cropImageFrame);
    CGFloat left = CGRectGetMinX(cropImageFrame);
    CGFloat right = CGRectGetMaxX(cropImageFrame);
    CGFloat top  = CGRectGetMinY(cropImageFrame);
    CGFloat bottom = CGRectGetMaxY(cropImageFrame);
    CGFloat maxRight = CGRectGetMaxX(self.photoCropScrollView.contentView.frame);
    CGFloat maxBottom = CGRectGetMaxY(self.photoCropScrollView.contentView.frame);
    //修复位置
    if (left<0.0) {
        left = 0.0;
    }
    if (top<0.0) {
        top = 0.0;
    }
    if (right > maxRight) {
        CGFloat fixSpace = right-maxRight;
        left -= fixSpace;
    }
    if (bottom > maxBottom) {
        CGFloat fixSpace = bottom-maxBottom;
        top -= fixSpace;
    }
    
    return CGRectApplyAffineTransform(CGRectMake(left*currentScale, top*currentScale, width*currentScale, height*currentScale), self.photoCropScrollView.contentView.transform);
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    return self.photoCropScrollView;
}

#pragma mark - lazy load.

- (TFPhotoCropScrollView *)photoCropScrollView {
    if (!_photoCropScrollView) {
        _photoCropScrollView = [[TFPhotoCropScrollView alloc] initWithFrame:self.bounds];
    }
    return _photoCropScrollView;
}

- (UIView *)cropBackView {
    if (!_cropBackView) {
        _cropBackView = [[UIView alloc] initWithFrame:self.bounds];
        _cropBackView.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.6];
    }
    return _cropBackView;
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
