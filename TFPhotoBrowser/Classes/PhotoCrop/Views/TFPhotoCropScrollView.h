//
//  TFPhotoCropScrollView.h
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2017/2/21.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TFPhotoCropContentView.h"

/**
 图片剪切显示的图片的ScrollView
 */
@interface TFPhotoCropScrollView : UIScrollView

/**
 图片内容view
 */
@property (nonatomic, strong, readonly, nonnull) TFPhotoCropContentView *contentView;

/**
 图片
 */
@property (nonatomic, strong, nonnull) UIImage *image;


@end
