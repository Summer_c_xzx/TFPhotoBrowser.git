//
//  TFPhotoCropContentView.h
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2017/2/21.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 图片剪切的内容view
 */
@interface TFPhotoCropContentView : UIView

/**
 图片对象
 */
@property (nonatomic, strong, nonnull) UIImage *image;

/**
 图片view
 */
@property (nonatomic, strong, readonly, nonnull) UIImageView *imageView;

@end
