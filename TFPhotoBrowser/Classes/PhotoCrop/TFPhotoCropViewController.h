//
//  TFPhotoCropViewController.h
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2017/2/21.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TFPhotoCropInfoModel.h"

@class TFPhotoCropViewController;

/**
 图片剪裁器代理
 */
@protocol TFPhotoCropViewControllerDelegate <NSObject>

/**
 图片剪裁剪裁框信息数据模型--剪裁边框view和透明路径

 @param photoCropViewController 图片剪裁视图控制器
 @return UIView
 */
- (nonnull TFPhotoCropInfoModel *)photoCropViewControllerCropInfoModel:(nonnull TFPhotoCropViewController *)photoCropViewController;

@optional;

/**
 图片剪裁器点击了取消按钮,若实现代理，viewController的返回需要自己实现

 @param photoCropViewController 图片剪裁
 */
- (void)photoCropViewControllerDidCancel:(nonnull TFPhotoCropViewController *)photoCropViewController;

/**
 图片剪裁器选择了剪裁图片，可通过TFPhotoCropViewController属性cropedImage获取剪裁后的图片，若实现代理，viewController的返回需要自己实现

 @param photoCropViewController 图片剪裁
 */
- (void)photoCropViewControllerDidFinish:(nonnull TFPhotoCropViewController *)photoCropViewController;

@end

@interface TFPhotoCropViewController : UIViewController

/**
 代理
 */
@property (nonatomic, weak, nullable) id<TFPhotoCropViewControllerDelegate> delegate;

/**
 需要剪裁的图片
 */
@property (nonatomic, strong, nonnull) UIImage *image;

/**
 剪裁过后的图片
 */
@property (nonatomic, strong, nonnull) UIImage *cropedImage;

@end
