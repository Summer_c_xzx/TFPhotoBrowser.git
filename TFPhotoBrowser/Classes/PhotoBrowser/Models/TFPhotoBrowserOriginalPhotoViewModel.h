//
//  TFPhotoBrowserOriginalPhotoViewModel.h
//  KSPhotoBrowserDemo
//
//  Created by TFAppleWork-Summer on 2017/4/24.
//  Copyright © 2017年 Kyle Sun. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 图片浏览器原始图片view模型，两个属性都必须赋值
 */
@interface TFPhotoBrowserOriginalPhotoViewModel : NSObject

/**
 原来的图片
 */
@property (nonatomic, strong, nonnull) UIImage *originalImage;

/**
 原来的图片view
 */
@property (nonatomic, weak, nullable) UIView *originalPhotoView;

@end
