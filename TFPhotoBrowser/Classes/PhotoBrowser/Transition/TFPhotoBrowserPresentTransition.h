//
//  TFPhotoBrowserPresentTransition.h
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/11/25.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 图片浏览器转场动画代理
 */
@protocol TFPhotoBrowserPresentTransitionDelegate <NSObject>

/**
 需要做动画，把动画的代理传出去

 @param transitionContext 转场上下文
 */
- (void)photoBrowserPresentTransitionAnimateTransition:(nonnull id<UIViewControllerContextTransitioning>)transitionContext duration:(NSTimeInterval)duration;

@end

/**
 图片浏览器转场动画工具类
 */
@interface TFPhotoBrowserPresentTransition : NSObject<UIViewControllerAnimatedTransitioning>

/**
 代理者
 */
@property (nonatomic, weak, nullable) id<TFPhotoBrowserPresentTransitionDelegate> delegate;

@end
