//
//  TFPhotoBrowserPresentTransition.m
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/11/25.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#import "TFPhotoBrowserPresentTransition.h"
#import "TFPhotoBrowserViewController.h"


@implementation TFPhotoBrowserPresentTransition

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    return 0.3;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    if ([self.delegate respondsToSelector:@selector(photoBrowserPresentTransitionAnimateTransition:duration:)]) {
        [self.delegate photoBrowserPresentTransitionAnimateTransition:transitionContext duration:[self transitionDuration:transitionContext]];
    }
}

@end
