//
//  TFPhotoBrowserViewController.h
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/11/22.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TFPhotoBrowserPublicProtocol.h"
#import "TFPhotoBrowserOriginalPhotoViewModel.h"
#import "TFPhotoBrowserNavigationBar.h"

@class TFPhotoBrowserViewController;

/**
 图片浏览器数据源
 */
@protocol TFPhotoBrowserViewControllerDataSource <NSObject>

/**
 多少张图片

 @param photoBrowserViewController 图片浏览器
 @return 图片数量
 */
- (NSInteger)numberOfPhotosInPhotoBrowserViewController:(nonnull TFPhotoBrowserViewController *)photoBrowserViewController;

/**
 根据索引返回图片对象，支持NSString--图片路径，图片名字，图片网络地址，PHAsset--本地图片、NSURL--网络地址
 
 @param photoBrowserViewController 图片浏览器
 @param index 索引
 @return 图片对象
 */
- (nonnull id)photoBrowserViewController:(nonnull TFPhotoBrowserViewController *)photoBrowserViewController photoForIndex:(NSInteger)index;

@optional;
/**
 图片浏览器显示的图片对应的上级页面显示图片的view，此方法用来实现图片显示转场切换的动画效果，会在转场显示和消失时调用

 @param photoBrowserViewController 图片浏览器
 @param index 索引
 @return TFPhotoBrowserOriginalPhotoViewModel 数据模型
 */
- (nullable TFPhotoBrowserOriginalPhotoViewModel *)photoBrowserViewController:(nonnull TFPhotoBrowserViewController *)photoBrowserViewController originalPhotoViewAtIndex:(NSInteger)index;

/**
 图片浏览器自定义cell（需服从TFPhotoBrowserDisplayCellPublic协议），通过该代理传入新的cell class.

 @param photoBrowserViewController 图片浏览器
 @return Class
 */
- (nullable Class)photoBrowserViewControllerCellClassForDisplay:(nonnull TFPhotoBrowserViewController *)photoBrowserViewController;

/**
 图片浏览器自定义视频cell（需服从TFPhotoBrowserDisplayCellPublic协议），通过该代理传入新的cell class.
 
 @param photoBrowserViewController 图片浏览器
 @return Class
 */
- (nullable Class)photoBrowserViewControllerVideoCellClassForDisplay:(nonnull TFPhotoBrowserViewController *)photoBrowserViewController;

@end

/**
 图片浏览器代理
 */
@protocol TFPhotoBrowserViewControllerDelegate <NSObject>

@optional;
/**
 图片浏览器器自定义底部view，若此代理不实现，则会默认创建一个bottomBar,
 
 @param photoBrowserViewController 图片浏览器
 @warning 会默认给bottomView添加距底部为0，左右边距为0的约束，自定义高度需要设置bottomView的frame中的高度，若为0默认为49.0
 @return UIView
 */
- (nullable UIView *)photoBrowserViewControllerCustomBottomView:(nonnull TFPhotoBrowserViewController *)photoBrowserViewController;

/**
 底部工具栏已经创建之后，在此代理中修改bottomBar的item，等等
 
 @param photoBrowserViewController 图片浏览器
 @param bottomBar 底部工具栏，默认在最右边会有一个系统的分享按钮
 */
- (void)photoBrowserViewController:(nonnull TFPhotoBrowserViewController *)photoBrowserViewController didCreatedBottomBar:(nonnull UIToolbar *)bottomBar;

/**
 创建顶部导航栏之后调用，在此代理中修改NavigationBar的item，等等。

 @param photoBrowserViewController 图片浏览器
 @param navigationBar 顶部导航栏
 */
- (void)photoBrowserViewController:(nonnull TFPhotoBrowserViewController *)photoBrowserViewController didCreatedNaivgationBar:(nonnull UINavigationBar *)navigationBar;

/**
 自定义页面的约束布局

 @param photoBrowserViewController 图片浏览器
 @param navigationBar 顶部导航栏
 @param collectionView 列表视图
 @param bottomView 底部view
 */
- (void)photoBrowserViewController:(nonnull TFPhotoBrowserViewController *)photoBrowserViewController customConstraintsWithSubView:(nonnull UINavigationBar *)navigationBar collectionView:(nonnull UICollectionView *)collectionView bottomView:(nonnull UIView *)bottomView;

/**
 图片浏览器ViewDidLoad的时候调用，用于添加自定义view

 @param photoBrowserViewController 图片浏览器
 */
- (void)photoBrowserViewControllerViewDidLoad:(nonnull TFPhotoBrowserViewController *)photoBrowserViewController;

/**
 图片浏览器将要消失时调用

 @param photoBrowserViewController 图片浏览器
 */
- (void)photoBrowserViewControllerWillDismiss:(nonnull TFPhotoBrowserViewController *)photoBrowserViewController;

/**
 图片浏览器消失时调用

 @param photoBrowserViewController 图片浏览器
 */
- (void)photoBrowserViewControllerDidDismiss:(nonnull TFPhotoBrowserViewController *)photoBrowserViewController;

/**
 图片浏览器做导航栏和底部工具栏移动动画时调用

 @param photoBrowserViewController 图片浏览器
 @param hidden 显示还是消失
 */
- (void)photoBrowserViewController:(nonnull TFPhotoBrowserViewController *)photoBrowserViewController navigationBarAndBottomBarDoMoveAnimationWithHidden:(BOOL)hidden;

/**
 图片浏览器重载cell时调用
 
 @param photoBrowserViewController 图片浏览器
 @param cell cell
= */
- (void)photoBrowserViewController:(nonnull TFPhotoBrowserViewController *)photoBrowserViewController didReloadCell:(nonnull UICollectionViewCell<TFPhotoBrowserDisplayCellPublic> *)cell;

/**
 图片浏览器中cell加载图片时调用

 @param photoBrowserViewController 图片浏览器
 @param cell UICollectionViewCell<TFPhotoBrowserDisplayCellPublic>
 */
- (void)photoBrowserViewController:(nonnull TFPhotoBrowserViewController *)photoBrowserViewController cellDidLoadPhoto:(nonnull UICollectionViewCell<TFPhotoBrowserDisplayCellPublic> *)cell;

/**
 图片浏览器中cell完成加载时调用

 @param photoBrowserViewController 图片浏览器
 @param cell UICollectionViewCell<TFPhotoBrowserDisplayCellPublic>
 */
- (void)photoBrowserViewController:(nonnull TFPhotoBrowserViewController *)photoBrowserViewController cellDidFinishLoadPhoto:(nonnull UICollectionViewCell<TFPhotoBrowserDisplayCellPublic> *)cell;

/**
 图片浏览器中cell清空图片时调用

 @param photoBrowserViewController 图片浏览器
 @param cell UICollectionViewCell<TFPhotoBrowserDisplayCellPublic>
 */
- (void)photoBrowserViewController:(nonnull TFPhotoBrowserViewController *)photoBrowserViewController cellDidClearLoadPhoto:(nonnull UICollectionViewCell<TFPhotoBrowserDisplayCellPublic> *)cell;

/**
 图片浏览器中cell完成从ICloud加载图片时调用
 
 @param photoBrowserViewController 图片浏览器
 @param cell UICollectionViewCell<TFPhotoBrowserDisplayCellPublic>
 */
- (void)photoBrowserViewController:(nonnull TFPhotoBrowserViewController *)photoBrowserViewController cellDidFinishLoadPhotoFromICloud:(nonnull UICollectionViewCell<TFPhotoBrowserDisplayCellPublic> *)cell;

/**
 图片浏览器中最终展示某个cell时调用

 @param photoBrowserViewController 图片浏览器
 @param cell UICollectionViewCell<TFPhotoBrowserDisplayCellPublic>
 */
- (void)photoBrowserViewController:(nonnull TFPhotoBrowserViewController *)photoBrowserViewController didDisplayCell:(nonnull UICollectionViewCell<TFPhotoBrowserDisplayCellPublic> *)cell;

/**
 图片浏览器中将要展示某个cell时调用
 
 @param photoBrowserViewController 图片浏览器
 @param cell UICollectionViewCell<TFPhotoBrowserDisplayCellPublic>
 */
- (void)photoBrowserViewController:(nonnull TFPhotoBrowserViewController *)photoBrowserViewController willDisplayCell:(nonnull UICollectionViewCell<TFPhotoBrowserDisplayCellPublic> *)cell;

/**
 图片浏览器中结束展示某个cell时调用
 
 @param photoBrowserViewController 图片浏览器
 @param cell UICollectionViewCell<TFPhotoBrowserDisplayCellPublic>
 */
- (void)photoBrowserViewController:(nonnull TFPhotoBrowserViewController *)photoBrowserViewController didEndDisplayCell:(nonnull UICollectionViewCell<TFPhotoBrowserDisplayCellPublic> *)cell;


@end

/**
 导航栏背景样式
 */
typedef NS_ENUM(NSUInteger, TFPhotoBrowserTopNavigationBarBackgroundStyle) {
    /**
     导航栏背景样式--黑色
     */
    TFPhotoBrowserTopNavigationBarBackgroundStyleBlack,
    /**
     导航栏背景样式--默认背景
     */
    TFPhotoBrowserTopNavigationBarBackgroundStyleDefaultAppearance,
};

/**
 图片浏览器
 */
@interface TFPhotoBrowserViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,TFPhotoBrowserDisplayCellDelegate>

/**
 数据源
 */
@property (nonatomic, weak, nullable) id<TFPhotoBrowserViewControllerDataSource> dataSource;

/**
 代理
 */
@property (nonatomic, weak, nullable) id<TFPhotoBrowserViewControllerDelegate> delegate;

/**
 顶部导航栏Item
 */
@property (nonatomic, strong, readonly, nonnull) UINavigationItem *topNavigationItem;

/**
 顶部导航栏
 */
@property (nonatomic, strong, readonly, nonnull) TFPhotoBrowserNavigationBar *navigationBar;

/**
 导航栏是否隐藏
 */
@property (nonatomic, assign, readonly) BOOL topNavigationBarHidden;

/**
 顶部导航栏默认背景样式
 */
@property (nonatomic, assign) TFPhotoBrowserTopNavigationBarBackgroundStyle topNavigationBarBackgroundStyle;

/**
 图片展示的collection View
 */
@property (nonatomic, strong, readonly, nonnull) UICollectionView *collectionView;

/**
 底部工具栏
 */
@property (nonatomic, strong, readonly, nonnull) UIView *bottomView;

/**
 当前图片位置索引
 */
@property (nonatomic, assign) NSInteger currentPage;

/**
 重载PhotoBrowser
 */
- (void)reload;

/**
 给子view添加约束，子类可重写此方法定义自己的布局约束
 */
- (void)addConstraintsForSubViews;

/**
 图片浏览器cell class

 @return Class
 */
- (nonnull Class)photoBrowserCellClass;

/**
 图片浏览器视频cell class

 @return Class
 */
- (nonnull Class)photoBrowserVideoCellClass;

/**
 设置bars隐藏和显示
 
 @param hidden 隐藏或者显示
 */
- (void)setBarsHidden:(BOOL)hidden;

/**
 获取默认的底部view

 @return UIView
 */
- (nonnull UIView *)getDefaultBottomView;

/**
 根据索引获取对应的展示cell

 @param index 索引
 @return 展示cell
 */
- (nonnull UICollectionViewCell<TFPhotoBrowserDisplayCellPublic> *)displayCellAtIndex:(NSInteger)index;

/**
 根据cell获取当前索引

 @param cell UICollectionViewCell<TFPhotoBrowserDisplayCellPublic>
 @return NSInteger 索引
 */
- (NSInteger)indexForCell:(nonnull UICollectionViewCell<TFPhotoBrowserDisplayCellPublic> *)cell;

/**
 获取当前展示的cell

 @return 当前展示的cell
 */
- (nonnull UICollectionViewCell<TFPhotoBrowserDisplayCellPublic> *)currentDisplayCell;

@end
