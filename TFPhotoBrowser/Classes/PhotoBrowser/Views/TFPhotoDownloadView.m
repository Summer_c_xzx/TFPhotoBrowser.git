//
//  TFPhotoDownloadView.m
//  TFPhotoBrowser
//
//  Created by Summer on 2018/2/1.
//

#import "TFPhotoDownloadView.h"

@interface TFPhotoDownloadView ()<CAAnimationDelegate>

@property (nonatomic, strong) CAShapeLayer *progressLayer;

@end

@implementation TFPhotoDownloadView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _space = 2.0;
        _progressTintColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
        self.layer.cornerRadius = CGRectGetHeight(frame)*0.5;
        self.layer.masksToBounds = YES;
        self.layer.borderColor = _progressTintColor.CGColor;
        self.layer.borderWidth = 1.0;
        [self.layer addSublayer:self.progressLayer];
        self.progressLayer.strokeColor = _progressTintColor.CGColor;
        self.progressLayer.strokeEnd = 0.0;
    }
    return self;
}

- (void)setProgress:(float)progress {
    _progress = progress;
    self.progressLayer.strokeEnd = progress;
}

- (void)setProgressTintColor:(UIColor *)progressTintColor {
    _progressTintColor = progressTintColor;
    self.progressLayer.strokeColor = progressTintColor.CGColor;
    self.layer.borderColor = progressTintColor.CGColor;
}

#pragma mark - lazy load.

- (CAShapeLayer *)progressLayer {
    if (!_progressLayer) {
        _progressLayer = [CAShapeLayer layer];
        CGFloat radius = (CGRectGetWidth(self.frame)-_space*2)*0.5;
        CGFloat pieRadius = radius *0.5;
        CGPoint center = CGPointMake(CGRectGetWidth(self.frame)*0.5, CGRectGetHeight(self.frame)*0.5);
        UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:center radius:pieRadius startAngle:-M_PI_2 endAngle:M_PI_2 * 3 clockwise:YES];
        _progressLayer.fillColor = [UIColor clearColor].CGColor;
        _progressLayer.lineWidth = radius;
        _progressLayer.path = path.CGPath;
    }
    return _progressLayer;
}

@end
