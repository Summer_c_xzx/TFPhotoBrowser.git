//
//  TFPhotoBrowserVideoPlayView.h
//  Pods
//
//  Created by TFAppleWork-Summer on 2017/8/24.
//
//

#import <UIKit/UIKit.h>

@class AVPlayer;
@class AVPlayerLayer;

/**
 图片浏览器视频播放view
 */
@interface TFPhotoBrowserVideoPlayView : UIView

/**
 初始化方法

 @param player 播放器
 @return TFPhotoBrowserVideoPlayView
 */
- (nonnull instancetype)initWithPlayer:(nonnull AVPlayer *)player;

/**
 播放器
 */
@property (nonatomic, strong, nonnull) AVPlayer *player;

/**
 视频显示layer
 */
@property (nonatomic, strong, readonly, nonnull) AVPlayerLayer *playerLayer;

/**
 播放，请调用此方法替代直接调用AVPlayer的play方法
 */
- (void)play;

/**
 暂停，请调用此方法替代直接调用AVPlayer的pause方法
 */
- (void)pause;

/**
 重新开始
 */
- (void)resume;

@end
