//
//  TFPhotoBrowserPublicProtocol.h
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2017/2/9.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#ifndef TFPhotoBrowserPublicProtocol_h
#define TFPhotoBrowserPublicProtocol_h
#import "TFPhotoScrollView.h"
#import "TFPhotoBrowserVideoPlayView.h"

@class PHAsset;

@protocol TFPhotoBrowserDisplayCellDelegate;

/**
 图片浏览器显示cell必须要服从的协议
 */
@protocol TFPhotoBrowserDisplayCellPublic <NSObject>

/**
 图片或者视频对象，需要调用loadPhoto去加载图片
 */
@property (nonatomic, strong, nonnull) id photo;

/**
 显示的图片对象
 */
@property (nonatomic, strong, nullable) UIImage *image;

/**
 显示图片的photoScrollView
 */
@property (nonatomic, strong, readonly, nullable) TFPhotoScrollView *photoScrollView;

/**
 显示视频的view
 */
@property (nonatomic, strong, readonly, nullable) TFPhotoBrowserVideoPlayView *videoPlayView;

/**
 索引
 */
@property (nonatomic, strong, nullable) NSIndexPath *indexPath;

/**
 代理
 */
@property (nonatomic, weak, nullable) id<TFPhotoBrowserDisplayCellDelegate> delegate;

/**
 是不是正在加载
 */
@property (nonatomic, assign) BOOL isLoading;

/**
 加载图片
 */
- (void)loadPhoto;

/**
 清空图片
 */
- (void)clearPhoto;

@end

/**
 图片浏览器显示cell的代理
 */
@protocol TFPhotoBrowserDisplayCellDelegate <NSObject>

@optional;

/**
 图片浏览器cell开始加载图片时调用

 @param cell UICollectionViewCell<TFPhotoBrowserDisplayCellPublic>
 */
- (void)photoBrowserDisplayCellDidLoadPhoto:(__kindof  UICollectionViewCell<TFPhotoBrowserDisplayCellPublic> * __nonnull)cell;

/**
 图片浏览器cell开始清空图片时调用

 @param cell UICollectionViewCell<TFPhotoBrowserDisplayCellPublic>
 */
- (void)photoBrowserDisplayCellDidClearPhoto:(__kindof UICollectionViewCell<TFPhotoBrowserDisplayCellPublic> * __nonnull)cell;

/**
 图片浏览器cell加载完图片时调用
 
 @param cell UICollectionViewCell<TFPhotoBrowserDisplayCellPublic>
 */
- (void)photoBrowserDisplayCellDidFinishLoadPhoto:(__kindof UICollectionViewCell<TFPhotoBrowserDisplayCellPublic> * __nonnull)cell;

/**
 图片浏览器cell完成从ICloud加载图片时调用

 @param cell UICollectionViewCell<TFPhotoBrowserDisplayCellPublic>
 */
- (void)photoBrowserDisplayCellDidFinishLoadPhotoFromICloud:(__kindof UICollectionViewCell<TFPhotoBrowserDisplayCellPublic> * __nonnull)cell;

@end

#endif /* TFPhotoBrowserPublicProtocol_h */
