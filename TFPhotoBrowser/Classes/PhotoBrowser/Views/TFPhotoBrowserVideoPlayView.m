//
//  TFPhotoBrowserVideoPlayView.m
//  Pods
//
//  Created by TFAppleWork-Summer on 2017/8/24.
//
//

#import "TFPhotoBrowserVideoPlayView.h"
//util
#import <AVFoundation/AVFoundation.h>
#import "TFPhotoResourceHelper.h"

@interface TFPhotoBrowserVideoPlayView ()

@property (nonatomic, strong) UIButton *playButton;

@end

@implementation TFPhotoBrowserVideoPlayView

- (instancetype)initWithPlayer:(AVPlayer *)player {
    self = [super init];
    if (self) {
        self.player = player;
        self.playerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
        [self addSubview:self.playButton];
        
        _playButton.translatesAutoresizingMaskIntoConstraints = NO;
        
        [NSLayoutConstraint constraintWithItem:_playButton attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0].active = YES;
        [NSLayoutConstraint constraintWithItem:_playButton attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0].active = YES;

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(p_didFinishedPlayVideo:) name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
        
    }
    return self;
}

- (void)setPlayer:(AVPlayer *)player {
    _player = player;
    self.playerLayer.player = player;
}

- (void)play {
    [self.player play];
    self.playButton.selected = YES;
}

- (void)pause {
    [self.player pause];
    self.playButton.selected = NO;
}

- (void)resume {
    [self.player.currentItem seekToTime:kCMTimeZero];
    self.playButton.selected = NO;
}

- (AVPlayerLayer *)playerLayer {
    return (AVPlayerLayer *)self.layer;
}

+ (Class)layerClass {
    return [AVPlayerLayer class];
}

- (void)p_playButtonAction:(UIButton *)sender {
    if (self.player.currentItem.status==AVPlayerItemStatusReadyToPlay) {
        sender.selected = !sender.selected;
        if (sender.selected) {
            [self.player play];
        }
        else {
            [self.player pause];
        }
    }
}

#pragma mark - noti actions.

- (void)p_didFinishedPlayVideo:(NSNotification *)noti {
    [self resume];
}

#pragma mark - lazy load.

- (UIButton *)playButton {
    if (!_playButton) {
        _playButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_playButton setImage:[TFPhotoResourceHelper imageNamed:@"photo_video_play"] forState:UIControlStateNormal];
        [_playButton setImage:[TFPhotoResourceHelper imageNamed:@"photo_video_pause"] forState:UIControlStateSelected];
        [_playButton addTarget:self action:@selector(p_playButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _playButton;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
