//
//  TFPhotoScrollView.h
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/11/22.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 展示图片的ScrollView
 */
@interface TFPhotoScrollView : UIScrollView

/**
 可缩放的图片view
 */
@property (nonatomic, strong, readonly, nonnull) UIImageView *zoomImageView;

/**
 显示的图片
 */
@property (nonatomic, strong, nullable) UIImage *image;

@end
