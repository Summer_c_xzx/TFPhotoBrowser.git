//
//  TFPhotoDownloadView.h
//  TFPhotoBrowser
//
//  Created by Summer on 2018/2/1.
//

#import <UIKit/UIKit.h>

/**
 图片下载view
 */
@interface TFPhotoDownloadView : UIView

/**
 有进度时的颜色
 */
@property (nonatomic, strong, nonnull) UIColor *progressTintColor;

/**
 距离
 */
@property (nonatomic, assign) CGFloat space;

/**
 进度
 */
@property (nonatomic, assign) float progress;


@end
