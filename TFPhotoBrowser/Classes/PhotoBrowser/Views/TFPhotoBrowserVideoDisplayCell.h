//
//  TFPhotoBrowserVideoDisplayCell.h
//  Pods
//
//  Created by TFAppleWork-Summer on 2017/8/24.
//
//

#import <UIKit/UIKit.h>
#import "TFPhotoBrowserPublicProtocol.h"

/**
 图片浏览器显示视频cell
 */
@interface TFPhotoBrowserVideoDisplayCell : UICollectionViewCell<TFPhotoBrowserDisplayCellPublic>

@property (nonatomic, strong) PHAsset *photo;


@end
