//
//  TFPhotoBrowserNavigationBarView.m
//  TFPhotoBrowser
//
//  Created by TFAppleWork-Summer on 2017/9/25.
//

#import "TFPhotoBrowserNavigationBar.h"

@implementation TFPhotoBrowserNavigationBar

#pragma mark - lazy load.

- (CGSize)sizeThatFits:(CGSize)size {
    CGSize sizeThatFit = [super sizeThatFits:size];
    CGFloat height = CGRectGetHeight([UIApplication sharedApplication].statusBarFrame)+44.0;
    sizeThatFit.height = height;
    return sizeThatFit;
}

- (void)setFrame:(CGRect)frame {
    CGFloat height = CGRectGetHeight([UIApplication sharedApplication].statusBarFrame)+44.0;
    frame.size.height = height;
    [super setFrame:frame];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if ([UIApplication sharedApplication].isStatusBarHidden) {
        return;
    }
    CGFloat height = CGRectGetHeight([UIApplication sharedApplication].statusBarFrame)+44.0;
    for (UIView *subview in self.subviews) {
        NSString* subViewClassName = NSStringFromClass([subview class]);
        if ([subViewClassName containsString:@"UIBarBackground"]) {
            subview.frame = CGRectMake(0, 0, CGRectGetWidth(self.bounds), height);
        }else if ([subViewClassName containsString:@"UINavigationBarContentView"]) {
            CGRect subViewFrame = subview.frame;
            if (CGRectGetHeight(subview.frame) < height) {
                subViewFrame.origin.y = height - CGRectGetHeight(subview.frame);
            }else {
                subViewFrame.origin.y = 0;
            }
            subview.frame = subViewFrame;
        }
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
