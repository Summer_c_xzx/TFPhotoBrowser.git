//
//  TFPhotoHandler.m
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/11/15.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#import "TFPhotoHandler.h"
#import "TFPhotoCacheOperation.h"
#import <AssetsLibrary/AssetsLibrary.h>

@import Darwin;

@interface TFPhotoHandler ()

@property (nonatomic, strong) PHCachingImageManager *cacheImageManager;

@property (nonatomic, strong) PHImageRequestOptions *imageRequestOptions;

@property (nonatomic, strong) PHVideoRequestOptions *videoRequestOptions;

@property (nonatomic, strong) NSOperationQueue *cacheOperationQueue;

@end

@implementation TFPhotoHandler

+ (instancetype)sharedHandler {
    static dispatch_once_t onceToken;
    static TFPhotoHandler *sharedHandler = nil;
    dispatch_once(&onceToken, ^{
        sharedHandler = [[TFPhotoHandler alloc] init];
    });
    return sharedHandler;
}

+ (void)checkAuthorizationStatusWithResult:(void (^)(BOOL))result {
    
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    if (status != PHAuthorizationStatusAuthorized) {
        //判断是否已询问授权
        if (status==PHAuthorizationStatusNotDetermined) {
            //请求授权
            [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                //回到主线程
                dispatch_async(dispatch_get_main_queue(), ^{
                    switch (status) {
                        case PHAuthorizationStatusAuthorized:
                        {
                            //已授权
                            if (result) {
                                result(YES);
                            }
                        }
                            break;
                        case PHAuthorizationStatusRestricted:
                        case PHAuthorizationStatusDenied: {
                            //未授权
                            if (result) {
                                result(NO);
                            }
                        }
                            break;
                        default:
                            break;
                    }
                });
            }];
        }
        else {
            //未授权
            if (result) {
                result(NO);
            }
        }
    }
    else {
        if (result) {
            result(YES);
        }
    }
}

+ (void)enumerateAllValidPHAssetCollection:(void (^)(PHAssetCollection *))enumrateCollection {
    PHFetchOptions *options = [[PHFetchOptions alloc] init];
    NSArray *listArr = @[
                         [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeSmartAlbumUserLibrary options:options],
                         [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeSmartAlbumFavorites options:options],
                         [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeSmartAlbumGeneric options:options],
                         [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeSmartAlbumSelfPortraits options:options],
                         [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeSmartAlbumPanoramas options:options],
                         [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeSmartAlbumVideos options:options],
                         [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeSmartAlbumTimelapses options:options],
                         [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeSmartAlbumBursts options:options],
                         [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeSmartAlbumSlomoVideos options:options],
                         [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeSmartAlbumScreenshots options:options],
                         [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeSmartAlbumAllHidden options:options],
                         [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeSmartAlbumRecentlyAdded options:options],
                         
                         [PHCollectionList fetchCollectionListsWithType:PHCollectionListTypeSmartFolder subtype:PHCollectionListSubtypeAny options:options],
                         
                         [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAlbumCloudShared options:options],
                         [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAlbumMyPhotoStream options:options],
                         
                         [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAlbumSyncedAlbum options:options],
                         [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAlbumRegular options:options],
                         [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAlbumImported options:options],
                         ];
    for (PHFetchResult *result in listArr) {
        for (PHAssetCollection *collection in result) {
            if ([collection isKindOfClass:[PHAssetCollection class]]) {
                if (enumrateCollection) {
                    enumrateCollection(collection);
                }
            }
        }
    }
}

+ (PHFetchResult<PHAsset *> *)getAssetsWithCollection:(PHAssetCollection *)collection mediaTypes:(NSArray<NSNumber *> *)mediaTypes{
    PHFetchOptions *options = [[PHFetchOptions alloc] init];
    options.predicate = [NSPredicate predicateWithFormat:@"mediaType IN %@",mediaTypes];
    options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES]];
    PHFetchResult *assetsResult = [PHAsset fetchAssetsInAssetCollection:collection options:options];
    return assetsResult;
}

- (void)cacheThumbnailImageWithAssets:(NSArray<PHAsset *> *)assets size:(CGSize)size {
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        if (assets.count>50) {
            //采用分流方式去缓存图片
            //缓存图片
            NSInteger cacheLength = 50;
            NSInteger cacheTimes = assets.count/cacheLength;
            NSInteger remainCount = assets.count%cacheLength;
            if (remainCount) {
                cacheTimes += 1;
            }
            __block NSInteger cacheIndex = 0;
            typeof(self) __weak weakSelf = self;
            for (NSInteger i=0; i<cacheTimes; i++) {
                NSInteger currentLength = cacheLength;
                if (i==cacheTimes-1&&remainCount) {
                    currentLength = remainCount;
                }
                TFPhotoCacheOperation *operation = [[TFPhotoCacheOperation alloc] init];
                operation.task = ^() {
                    CGFloat cup_usage = [TFPhotoHandler cpu_usage];
                    if (cup_usage>0.10) {
                        sleep(2.0);
                    }
                    //缓存图片
                    [weakSelf.cacheImageManager startCachingImagesForAssets:assets targetSize:size contentMode:PHImageContentModeAspectFit options:self.imageRequestOptions];
                    cacheIndex += currentLength;
                    sleep(1.0);
                };
                [self.cacheOperationQueue addOperation:operation];
            }
            
        }
        else {
            [self.cacheImageManager startCachingImagesForAssets:assets targetSize:size contentMode:PHImageContentModeAspectFit options:self.imageRequestOptions];
        }
    });
}

- (void)getThumbnailImageWithAsset:(PHAsset *)asset size:(CGSize)size resultHandler:(TFPhotoRequestImageResultHandler)resultHandler {
    self.imageRequestOptions.networkAccessAllowed = YES;
    self.imageRequestOptions.progressHandler = nil;
    self.imageRequestOptions.synchronous = YES;
    [self.cacheImageManager requestImageForAsset:asset targetSize:size contentMode:PHImageContentModeAspectFit options:self.imageRequestOptions resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
        if (result) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (resultHandler) {
                    resultHandler(result,info);
                }
            });
        }
    }];
}

- (void)getOriginalImageWithAsset:(PHAsset *)asset synchronous:(BOOL)synchronous progressHandler:(PHAssetImageProgressHandler)progressHandler resultHandler:(TFPhotoRequestImageResultHandler)resultHandler {
    self.imageRequestOptions.networkAccessAllowed = NO;
    self.imageRequestOptions.synchronous = synchronous;
    self.imageRequestOptions.progressHandler = progressHandler;
    [self.cacheImageManager requestImageDataForAsset:asset options:self.imageRequestOptions resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
        UIImage *result = [UIImage imageWithData:imageData];
        if (synchronous) {
            if (resultHandler) {
                resultHandler(result,info);
            }
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (resultHandler) {
                    resultHandler(result,info);
                }
            });
        }
        
    }];
}

- (void)checkImageIsInICloudWithAsset:(PHAsset *)asset resultHandler:(TFPhotoRequestImageResultHandler)resultHandler {
    self.imageRequestOptions.networkAccessAllowed = NO;
    self.imageRequestOptions.synchronous = YES;
    self.imageRequestOptions.progressHandler = nil;
    [self.cacheImageManager requestImageDataForAsset:asset options:self.imageRequestOptions resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
        if (resultHandler) {
            resultHandler(nil,info);
        }
    }];
}

- (PHImageRequestID)downLoadICloudImageWithAsset:(PHAsset *)asset progressHandler:(PHAssetImageProgressHandler)progressHandler resultHandler:(TFPhotoRequestImageResultHandler)resultHandler {
    self.imageRequestOptions.networkAccessAllowed = YES;
    self.imageRequestOptions.synchronous = NO;
    self.imageRequestOptions.progressHandler = progressHandler;
    return [self.cacheImageManager requestImageDataForAsset:asset options:self.imageRequestOptions resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
        UIImage *iCloudImage = [UIImage imageWithData:imageData];
        if (iCloudImage) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (resultHandler) {
                    resultHandler(iCloudImage,info);
                }
            });
        }
    }];
}

- (void)getVideoAssetWithPHAsset:(PHAsset *)anAsset resultHandler:(TFPhotoRequestVideoResultHandler)resultHandler {
    self.videoRequestOptions.networkAccessAllowed = NO;
    [self.cacheImageManager requestAVAssetForVideo:anAsset options:self.videoRequestOptions resultHandler:^(AVAsset * _Nullable asset, AVAudioMix * _Nullable audioMix, NSDictionary * _Nullable info) {
        if (resultHandler) {
            resultHandler(asset,audioMix,info);
        }
    }];
}

- (PHImageRequestID)downLoadICloudVideoWithPHAsset:(PHAsset *)anAsset progressHandler:(PHAssetVideoProgressHandler)progressHandler resultHandler:(TFPhotoRequestVideoResultHandler)resultHandler {
    self.videoRequestOptions.networkAccessAllowed = YES;
    self.videoRequestOptions.progressHandler = progressHandler;
    return [self.cacheImageManager requestAVAssetForVideo:anAsset options:self.videoRequestOptions resultHandler:^(AVAsset * _Nullable asset, AVAudioMix * _Nullable audioMix, NSDictionary * _Nullable info) {
        if (resultHandler) {
            resultHandler(asset,audioMix,info);
        }
    }];
}

- (void)cancelICloudDownloadWith:(PHImageRequestID)requestID {
    [self.cacheImageManager cancelImageRequest:requestID];
}

- (void)clearCacheImagesWithAssets:(NSArray<PHAsset *> *)assets size:(CGSize)size {
    [self.cacheImageManager stopCachingImagesForAssets:assets targetSize:size contentMode:PHImageContentModeAspectFit options:self.imageRequestOptions];
}

- (void)clearAllCacheImages {
    [self.cacheOperationQueue cancelAllOperations];
    _cacheOperationQueue = nil;
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    if (status==PHAuthorizationStatusAuthorized) {
        [self.cacheImageManager stopCachingImagesForAllAssets];
    }
}


+ (float)cpu_usage
{
    kern_return_t			kr = { 0 };
    task_info_data_t		tinfo = { 0 };
    mach_msg_type_number_t	task_info_count = TASK_INFO_MAX;
    
    kr = task_info( mach_task_self(), TASK_BASIC_INFO, (task_info_t)tinfo, &task_info_count );
    if ( KERN_SUCCESS != kr )
        return 0.0f;
    
    task_basic_info_t		basic_info = { 0 };
    thread_array_t			thread_list = { 0 };
    mach_msg_type_number_t	thread_count = { 0 };
    
    thread_info_data_t		thinfo = { 0 };
    thread_basic_info_t		basic_info_th = { 0 };
    
    basic_info = (task_basic_info_t)tinfo;
    
    // get threads in the task
    kr = task_threads( mach_task_self(), &thread_list, &thread_count );
    if ( KERN_SUCCESS != kr )
        return 0.0f;
    
    long	tot_sec = 0;
    long	tot_usec = 0;
    float	tot_cpu = 0;
    
    for ( int i = 0; i < thread_count; i++ )
    {
        mach_msg_type_number_t thread_info_count = THREAD_INFO_MAX;
        
        kr = thread_info( thread_list[i], THREAD_BASIC_INFO, (thread_info_t)thinfo, &thread_info_count );
        if ( KERN_SUCCESS != kr )
            return 0.0f;
        
        basic_info_th = (thread_basic_info_t)thinfo;
        if ( 0 == (basic_info_th->flags & TH_FLAGS_IDLE) )
        {
            tot_sec = tot_sec + basic_info_th->user_time.seconds + basic_info_th->system_time.seconds;
            tot_usec = tot_usec + basic_info_th->system_time.microseconds + basic_info_th->system_time.microseconds;
            tot_cpu = tot_cpu + basic_info_th->cpu_usage / (float)TH_USAGE_SCALE;
        }
    }
    
    kr = vm_deallocate( mach_task_self(), (vm_offset_t)thread_list, thread_count * sizeof(thread_t) );
    if ( KERN_SUCCESS != kr )
        return 0.0f;
    
    return tot_cpu;
}

#pragma mark - lazy load.

- (PHCachingImageManager *)cacheImageManager {
    if (!_cacheImageManager) {
        _cacheImageManager = [[PHCachingImageManager alloc] init];
    }
    return _cacheImageManager;
}

- (PHImageRequestOptions *)imageRequestOptions {
    if (!_imageRequestOptions) {
        _imageRequestOptions = [[PHImageRequestOptions alloc] init];
        _imageRequestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
    }
    return _imageRequestOptions;
}

- (PHVideoRequestOptions *)videoRequestOptions {
    if (!_videoRequestOptions) {
        _videoRequestOptions = [[PHVideoRequestOptions alloc] init];
        _videoRequestOptions.deliveryMode = PHVideoRequestOptionsDeliveryModeAutomatic;
        _videoRequestOptions.networkAccessAllowed = YES;
    }
    return _videoRequestOptions;
}

- (NSOperationQueue *)cacheOperationQueue {
    if (!_cacheOperationQueue) {
        _cacheOperationQueue = [[NSOperationQueue alloc]init];
        _cacheOperationQueue.qualityOfService = NSQualityOfServiceUtility;
        _cacheOperationQueue.maxConcurrentOperationCount = 1;
    }
    return _cacheOperationQueue;
}

@end
