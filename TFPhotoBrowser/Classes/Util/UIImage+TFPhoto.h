//
//  UIImage+TFPhoto.h
//  KSPhotoBrowserDemo
//
//  Created by TFAppleWork-Summer on 2017/4/24.
//  Copyright © 2017年 Kyle Sun. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 UIImage类别
 */
@interface UIImage (TFPhoto)

/**
 获取修复位置后的图片

 @return 修复位置后的图片
 */
- (nonnull UIImage *)tfPhoto_fixedOrientationImage;

@end
