//
//  UIColor+TFPhoto.m
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/11/14.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#import "UIColor+TFPhoto.h"

@implementation UIColor (TFPhoto)

+ (UIColor *)tfPhoto_hexColorWith:(NSString *)hexStr {
    return [UIColor tfPhoto_hexColorWith:hexStr alpha:1.0];
}

+ (UIColor *)tfPhoto_hexColorWith:(NSString *)hexStr alpha:(CGFloat)alpha {
    //剔除#
    hexStr = [hexStr stringByReplacingOccurrencesOfString:@"#" withString:@""];
    unsigned int red, green, blue;
    NSRange range;
    range.length =2;
    
    range.location =0;
    [[NSScanner scannerWithString:[hexStr substringWithRange:range]]scanHexInt:&red];
    range.location =2;
    [[NSScanner scannerWithString:[hexStr substringWithRange:range]]scanHexInt:&green];
    range.location =4;
    [[NSScanner scannerWithString:[hexStr substringWithRange:range]]scanHexInt:&blue];
    
    return [UIColor colorWithRed:(float)(red/255.0f)
                           green:(float)(green/255.0f)
                            blue:(float)(blue/255.0f)
                           alpha:alpha];
}

+ (UIColor *)tfPhoto_gradientColorFromColor:(UIColor *)color1 toColor:(UIColor *)color2 withHeight:(CGFloat)height {
    CGSize size = CGSizeMake(1, height);
    UIGraphicsBeginImageContextWithOptions(size, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
    
    NSArray* colors = [NSArray arrayWithObjects:(id)color1.CGColor, (id)color2.CGColor, nil];
    CGGradientRef gradient = CGGradientCreateWithColors(colorspace, (__bridge CFArrayRef)colors, NULL);
    CGContextDrawLinearGradient(context, gradient, CGPointMake(0, 0), CGPointMake(0, size.height), 0);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorspace);
    UIGraphicsEndImageContext();
    
    return [UIColor colorWithPatternImage:image];
}

@end
