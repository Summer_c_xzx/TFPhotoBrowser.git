//
//  UIColor+TFPhoto.h
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/11/14.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 UIColor类别
 */
@interface UIColor (TFPhoto)

/**
 通过hex值创建color

 @param hexStr hexString
 @return UIColor
 */
+ (nonnull UIColor *)tfPhoto_hexColorWith:(nonnull NSString *)hexStr;

/**
 通过hex值和透明度创建color，

 @param hexStr hexString
 @param alpha 透明度
 @return UIColor
 */
+ (nonnull UIColor *)tfPhoto_hexColorWith:(nonnull NSString *)hexStr alpha:(CGFloat)alpha;

/**
 通过两个颜色和高度创建渐变的颜色

 @param color1 开始颜色
 @param color2 最后颜色
 @param height 渐变高度
 @return UIColor
 */
+ (nonnull UIColor *)tfPhoto_gradientColorFromColor:(nonnull UIColor *)color1 toColor:(nonnull UIColor *)color2 withHeight:(CGFloat)height;

@end
