//
//  TFPhotoCacheOperation.h
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/12/7.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 图片缓存队列任务block
 */
typedef void(^TFPhotoCacheOperationTask)(void);

/**
 图片缓存队列
 */
@interface TFPhotoCacheOperation : NSOperation

/**
 任务block
 */
@property (nonatomic, copy, nullable) TFPhotoCacheOperationTask task;

@end
