//
//  TFPhotoHandler.h
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/11/15.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Photos/Photos.h>

/**
 请求图片结果block

 @param image 图片
 @param info 信息字典
 */
typedef void(^TFPhotoRequestImageResultHandler)(UIImage * __nullable image, NSDictionary * __nullable info);

/**
 请求视频结果block

 @param asset 视频asset
 @param audioMix 音频处理
 @param info 信息字典
 */
typedef void(^TFPhotoRequestVideoResultHandler)(AVAsset * __nullable asset, AVAudioMix * __nullable audioMix,NSDictionary * __nullable info);

/**
 图片处理工具类
 */
@interface TFPhotoHandler : NSObject

/**
 处理图片的单例

 @return 单例
 */
+ (nonnull instancetype)sharedHandler;

/**
 检测授权状态
 
 @param result 授权结果
 */
+ (void)checkAuthorizationStatusWithResult:(nullable void(^)(BOOL isAuthed))result;

/**
 遍历所有的有效的相册

 @param enumrateCollection 遍历块
 */
+ (void)enumerateAllValidPHAssetCollection:(nullable void(^)(PHAssetCollection * __nonnull collection))enumrateCollection;

/**
 获取相册中的PHAsset

 @param collection 相册集合
 @param mediaTypes 媒体类型
 @return PHFetchResult
 */
+ (nonnull PHFetchResult<PHAsset *> *)getAssetsWithCollection:(nonnull PHAssetCollection *)collection mediaTypes:(nonnull NSArray<NSNumber *> *)mediaTypes;

/**
 缓存缩略图

 @param assets PHAsset数组
 @param size 图片大小
 */
- (void)cacheThumbnailImageWithAssets:(nonnull NSArray<PHAsset *> *)assets size:(CGSize)size;

/**
 获取缓存缩略图

 @param asset PHAsset
 @param size 图片大小
 @param resultHandler 结果处理
 */
- (void)getThumbnailImageWithAsset:(nonnull PHAsset *)asset size:(CGSize)size resultHandler:(nullable TFPhotoRequestImageResultHandler)resultHandler;

/**
 获取原图

 @param asset PHAsset
 @param synchronous 是否同步执行
 @param resultHandler 结果处理
 */
- (void)getOriginalImageWithAsset:(nonnull PHAsset *)asset synchronous:(BOOL)synchronous progressHandler:(nullable PHAssetImageProgressHandler)progressHandler resultHandler:(nullable TFPhotoRequestImageResultHandler)resultHandler;

/**
 检测图片是否在ICloud上

 @param asset asset
 @param resultHandler 结果处理
 */
- (void)checkImageIsInICloudWithAsset:(nonnull PHAsset *)asset resultHandler:(nullable TFPhotoRequestImageResultHandler)resultHandler;

/**
 下载阿里云图片

 @param asset PHAsset
 @param progressHandler 进度处理
 @param resultHandler 结果处理
 
 @return PHImageRequestID
 */
- (PHImageRequestID)downLoadICloudImageWithAsset:(nonnull PHAsset *)asset progressHandler:(nullable PHAssetImageProgressHandler)progressHandler resultHandler:(nullable TFPhotoRequestImageResultHandler)resultHandler;

/**
 根据PHAsset获取本地视频asset
 
 @param anAsset PHAsset
 @param resultHandler 结果处理
 */
- (void)getVideoAssetWithPHAsset:(nonnull PHAsset *)anAsset resultHandler:(nullable TFPhotoRequestVideoResultHandler)resultHandler;

/**
 下载ICloud视频
 
 @param anAsset PHAsset
 @param progressHandler 进度处理
 @param resultHandler 结果处理
 
 @return PHImageRequestID
 */
- (PHImageRequestID)downLoadICloudVideoWithPHAsset:(nonnull PHAsset *)anAsset progressHandler:(nullable PHAssetVideoProgressHandler)progressHandler  resultHandler:(nullable TFPhotoRequestVideoResultHandler)resultHandler;

/**
 取消ICloud下载
 
 @param requestID 请求id
 */
- (void)cancelICloudDownloadWith:(PHImageRequestID)requestID;

/**
 清空保存在内存里的缓存图片
 
 @param assets PHAsset数组
 @param size 图片大小
 */
- (void)clearCacheImagesWithAssets:(nonnull NSArray<PHAsset *> *)assets size:(CGSize)size;

/**
 清空所有的缓存图片
 */
- (void)clearAllCacheImages;

@end
