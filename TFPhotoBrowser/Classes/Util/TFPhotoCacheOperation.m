//
//  TFPhotoCacheOperation.m
//  PhotoDemoProject
//
//  Created by TFAppleWork-Summer on 2016/12/7.
//  Copyright © 2016年 TFAppleWork-Summer. All rights reserved.
//

#import "TFPhotoCacheOperation.h"

@interface TFPhotoCacheOperation ()
{
    BOOL _executing;
    BOOL _finished;
}
@end

@implementation TFPhotoCacheOperation

- (void)start {
    if ([self isCancelled]|!_task) {
        [self willChangeValueForKey:@"isFinished"];
        _finished = YES;
        [self didChangeValueForKey:@"isFinished"];
        return;
    }
    else {
        [self willChangeValueForKey:@"isExecuting"];
        _executing = YES;
        [self didChangeValueForKey:@"isExecuting"];
        _task();
        [self completeOperation];
    }
}

- (void)completeOperation {
    [self willChangeValueForKey:@"isFinished"];
    [self willChangeValueForKey:@"isExecuting"];
    
    _executing = NO;
    _finished = YES;
    [self didChangeValueForKey:@"isExecuting"];
    [self didChangeValueForKey:@"isFinished"];
}

- (BOOL)isExecuting {
    return _executing;
}

- (BOOL)isFinished {
    return _finished;
}

- (BOOL)isAsynchronous {
    return YES;
}

@end
