//
//  ViewController.m
//  TFPhotoBrowserExample
//
//  Created by TFAppleWork-Summer on 2017/8/22.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import "ViewController.h"
//photoPicker
#import <TFPhotoBrowser/TFSinglePhotoPickerHelper.h>
#import <TFPhotoBrowser/TFPhotoPickerViewController.h>
#import "TFCustomPhotoPickerViewController.h"
//photoBrowser
#import <TFPhotoBrowser/TFPhotoBrowserViewController.h>
//photoCrop
#import <TFPhotoBrowser/TFPhotoCropViewController.h>

@interface ViewController ()<TFPhotoBrowserViewControllerDataSource,TFPhotoBrowserViewControllerDelegate,TFPhotoPickerViewControllerDelegate,TFSinglePhotoPickerHelperDelegate,TFPhotoCropViewControllerDelegate>

@property (nonatomic, strong) NSArray *dataArray;

@end

@implementation ViewController

- (instancetype)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _dataArray = @[
                   @{@"groupTitle":@"photoPicker",@"items":@[@"选择图片--单选",@"选择图片Present--多选MomentGroup样式",@"选择图片Push--多选CollectionList样式",@"选择图片Push--多选自定义样式"]},
                   @{@"groupTitle":@"photoBrowser",@"items":@[@"大图预览"]},
                   @{@"groupTitle":@"photoCrop",@"items":@[@"图片剪裁"]},
                   ];
    self.tableView.tableFooterView = [[UIView alloc] init];
}


#pragma mark - TableViewDataSource&Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *itemsArray = _dataArray[section][@"items"];
    return itemsArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSDictionary *dataDic = _dataArray[section];
    return dataDic[@"groupTitle"];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    NSArray *itemsArray = _dataArray[indexPath.section][@"items"];
    cell.textLabel.text = itemsArray[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        if (indexPath.row==0) {
            [TFSinglePhotoPickerHelper showSystemSinglePhotoPickerActionSheetInViewController:self withTitle:@"选择方式" infos:nil mediaTypes:@[(NSString *)kUTTypeMovie] allowsEditing:YES delegate:self];
        }
        else if (indexPath.row==1) {
            TFPhotoPickerViewController *photoPicker = [[TFPhotoPickerViewController alloc] initWithStyle:TFPhotoPickerStyleMomentGroup];
            photoPicker.mediaTypes = @[@(PHAssetMediaTypeImage)];
            UINavigationController *naVC = [[UINavigationController alloc] initWithRootViewController:photoPicker];
            photoPicker.selectMaxCount = 10;
            photoPicker.delegate = self;
            [self presentViewController:naVC animated:YES completion:nil];
        }
        else if (indexPath.row==2) {
            TFPhotoPickerViewController *photoPicker = [[TFPhotoPickerViewController alloc] initWithStyle:TFPhotoPickerStyleCollectionList];
            photoPicker.mediaTypes = @[@(PHAssetMediaTypeVideo)];
            photoPicker.selectMaxCount = 10;
            photoPicker.delegate = self;
            [self.navigationController pushViewController:photoPicker animated:YES];
        }
        else if (indexPath.row==3) {
            TFCustomPhotoPickerViewController *photoPicker = [[TFCustomPhotoPickerViewController alloc] initWithStyle:TFPhotoPickerStyleCollectionList];
            photoPicker.mediaTypes = @[@(PHAssetMediaTypeVideo)];
            photoPicker.selectMaxCount = 1;
            photoPicker.delegate = self;
            [self.navigationController pushViewController:photoPicker animated:YES];
        }
    }
    else if (indexPath.section==1) {
        TFPhotoBrowserViewController *photoBrowser = [[TFPhotoBrowserViewController alloc] init];
        photoBrowser.dataSource = self;
        photoBrowser.delegate = self;
        photoBrowser.topNavigationBarBackgroundStyle = TFPhotoBrowserTopNavigationBarBackgroundStyleBlack;
        [self presentViewController:photoBrowser animated:YES completion:nil];
    }
    else if (indexPath.section==2) {
        TFPhotoCropViewController *cropVC = [[TFPhotoCropViewController alloc] init];
        cropVC.delegate = self;
        cropVC.image = [UIImage imageNamed:@"crop_test"];
        [self.navigationController pushViewController:cropVC animated:YES];
    }
}

#pragma mark - TFSinglePhotoPickerHelperDelegate

- (void)singlePhotoPickerHelper:(TFSinglePhotoPickerHelper *)photoPickerHelper pickerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)singlePhotoPickerHelper:(TFSinglePhotoPickerHelper *)photoPickerHelper picker:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage *editImage = [TFSinglePhotoPickerHelper getOriginalImageWithInfo:info];
    [self p_setTableHeaderViewWithImage:editImage];
}

//- (TFPhotoCropInfoModel *)singlePhotoPickerHelper:(TFSinglePhotoPickerHelper *)photoPickerHelper pickerCustomCropInfoModel:(UIImagePickerController *)picker {
//    TFPhotoCropInfoModel *model = [[TFPhotoCropInfoModel alloc] init];
//    model.shapeType = TFPhotoCropShapeTypeRect;
//    model.size = CGSizeMake(100.0, 100.0);
//    model.borderWidth = 1.0;
//    return model;
//}

#pragma mark - TFPhotoBrowserViewControllerDataSource,TFPhotoBrowserViewControllerDelegate

- (NSInteger)numberOfPhotosInPhotoBrowserViewController:(TFPhotoBrowserViewController *)photoBrowserViewController {
    return 100;
}

- (id)photoBrowserViewController:(TFPhotoBrowserViewController *)photoBrowserViewController photoForIndex:(NSInteger)index {
    return [NSString stringWithFormat:@"https://s3.amazonaws.com/fast-image-cache/demo-images/FICDDemoImage%03zd.jpg", index];
}

#pragma mark - TFPhotoCropViewControllerDelegate

- (TFPhotoCropInfoModel *)photoCropViewControllerCropInfoModel:(TFPhotoCropViewController *)photoCropViewController {
    TFPhotoCropInfoModel *model = [[TFPhotoCropInfoModel alloc] init];
    model.shapeType = TFPhotoCropShapeTypeRect;
    model.size = CGSizeMake(339, 318);
    model.borderWidth = 1.0;
    return model;
}

- (void)photoCropViewControllerDidFinish:(TFPhotoCropViewController *)photoCropViewController {
    [self p_setTableHeaderViewWithImage:photoCropViewController.cropedImage];
    [photoCropViewController.navigationController popViewControllerAnimated:YES];
}

- (void)p_setTableHeaderViewWithImage:(UIImage *)image {
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100.0, 100.0)];
    imageView.image = image;
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, imageView.frame.size.height)];
    [headerView addSubview:imageView];
    imageView.center = headerView.center;
    self.tableView.tableHeaderView = headerView;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
