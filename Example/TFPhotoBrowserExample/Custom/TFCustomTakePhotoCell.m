//
//  TFCustomTakePhotoCell.m
//  TFPhotoBrowserExample
//
//  Created by TFAppleWork-Summer on 2017/8/22.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import "TFCustomTakePhotoCell.h"

@interface TFCustomTakePhotoCell ()

@property (nonatomic, strong) UIImageView *photoImageView;

@end

@implementation TFCustomTakePhotoCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self.contentView addSubview:self.photoImageView];
        
        _photoImageView.translatesAutoresizingMaskIntoConstraints = NO;
        NSDictionary *viewDic = @{@"photoImageView":_photoImageView};
        [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[photoImageView]|" options:0 metrics:nil views:viewDic]];
        [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[photoImageView]|" options:0 metrics:nil views:viewDic]];
    }
    return self;
}

#pragma mark - lazy load.

- (UIImageView *)photoImageView {
    if (!_photoImageView) {
        _photoImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"photo_camera_take"]];
        _photoImageView.contentMode = UIViewContentModeCenter;
        _photoImageView.clipsToBounds = YES;
        _photoImageView.backgroundColor = [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1.0];
    }
    return _photoImageView;
}


@end
