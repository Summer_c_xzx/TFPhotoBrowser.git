//
//  TFCustomPhotoPickerHeaderView.m
//  TFPhotoBrowserExample
//
//  Created by TFAppleWork-Summer on 2017/8/23.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import "TFCustomPhotoPickerHeaderView.h"

//util
#import <Masonry/Masonry.h>

@interface TFCustomPhotoPickerHeaderView ()

@property (nonatomic, strong) UIImageView *tipImageView;

@property (nonatomic, strong) UILabel *tipLabel;

@property (nonatomic, strong) UIButton *cancelButton;

@end

@implementation TFCustomPhotoPickerHeaderView

- (instancetype)init {
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:222/255.0 alpha:1.0];
        [self addSubview:self.tipImageView];
        [self addSubview:self.tipLabel];
        [self addSubview:self.cancelButton];
        
        [_tipImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(60.0);
            make.centerY.mas_equalTo(0.0);
        }];
        
        [_tipImageView setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];

        
        [_tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.tipImageView.mas_right).mas_offset(6.0);
            make.top.mas_equalTo(16.0);
            make.bottom.mas_equalTo(-16.0);
        }];
        
        [_cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.tipLabel.mas_right).mas_offset(6.0);
            make.right.mas_equalTo(-12.0);
            make.centerY.mas_equalTo(0.0);
        }];
        
        [_cancelButton setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    }
    return self;
}

- (void)setTip:(NSString *)tip {
    _tip = tip;
    _tipLabel.text = tip;
}

- (void)p_cancelButtonAction {
    if (_cancelButtonActionBlock) {
        _cancelButtonActionBlock();
    }
}

- (UIButton *)cancelButton {
    if (!_cancelButton) {
        _cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _cancelButton.contentEdgeInsets = UIEdgeInsetsMake(10.0, 10.0, 10.0, 0.0);
        [_cancelButton setImage:[UIImage imageNamed:@"photo_tip_close"] forState:UIControlStateNormal];
        [_cancelButton addTarget:self action:@selector(p_cancelButtonAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancelButton;
}

- (UILabel *)tipLabel {
    if (!_tipLabel) {
        _tipLabel = [[UILabel alloc] init];
        _tipLabel.textColor = [UIColor blackColor];
        _tipLabel.font = [UIFont systemFontOfSize:14];
        _tipLabel.numberOfLines = 0;
    }
    return _tipLabel;
}

- (UIImageView *)tipImageView {
    if (!_tipImageView) {
        _tipImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"photo_tip"]];
        _tipImageView.contentMode = UIViewContentModeScaleAspectFill;
        _tipImageView.clipsToBounds = YES;
    }
    return _tipImageView;
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
