//
//  TFCustomPhotoPickerViewController.m
//  TFPhotoBrowserExample
//
//  Created by TFAppleWork-Summer on 2017/8/22.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import "TFCustomPhotoPickerViewController.h"
//cell
#import "TFCustomTakePhotoCell.h"
//view
#import "TFCustomPhotoPickerHeaderView.h"
//util
#import <Masonry/Masonry.h>

@interface TFCustomPhotoPickerViewController ()

@property (nonatomic, strong) TFCustomPhotoPickerHeaderView *headerView;

@end

@implementation TFCustomPhotoPickerViewController

- (void)viewDidLoad {
    [self.view addSubview:self.headerView];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _headerView.tip = @"信息";
    
}

- (void)addConstraintsForSubViews {
    
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0.0);
        make.top.mas_equalTo(self.view.mas_topMargin);
    }];
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.headerView.mas_bottom).mas_offset(10.0);
        make.left.bottom.right.mas_equalTo(0.0);
    }];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(0.0);
        make.height.mas_equalTo(49.0);
        make.left.right.mas_equalTo(0.0);
    }];
}

- (void)p_cancelButtonAction {
    
    [_headerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0.0);
        make.top.mas_equalTo(64.0);
        make.height.mas_equalTo(0.0);
    }];
    
    [self.collectionView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(64.0);
        make.left.bottom.right.mas_equalTo(0.0);
    }];
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
        _headerView.hidden = YES;
    }];
    
}

- (TFCustomPhotoPickerHeaderView *)headerView {
    if (!_headerView) {
        _headerView = [[TFCustomPhotoPickerHeaderView alloc] init];
        typeof(self) __weak weakSelf = self;
        _headerView.cancelButtonActionBlock = ^() {
            [weakSelf p_cancelButtonAction];
        };
    }
    return _headerView;
}

//- (Class)photoPickerTakePhotoCellClass {
//    return [TFCustomTakePhotoCell class];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
