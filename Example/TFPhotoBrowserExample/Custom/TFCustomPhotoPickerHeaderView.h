//
//  TFCustomPhotoPickerHeaderView.h
//  TFPhotoBrowserExample
//
//  Created by TFAppleWork-Summer on 2017/8/23.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TFCustomPhotoPickerHeaderView : UIView

@property (nonatomic, strong) NSString *tip;

@property (nonatomic, copy) void(^cancelButtonActionBlock)(void);

@end
